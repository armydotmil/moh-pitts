//Last Updated 2014-07-16 1650 ASB
var CeremonyModel = Backbone.Model.extend({
	urlRoot: '//www.army.mil/medalofhonor/pitts/json/ceremony.json'
});

var CeremonyCollection = Backbone.Collection.extend({
	model: CeremonyModel,
	url: '//www.army.mil/medalofhonor/pitts/json/ceremony.json'
});

var ceremonyView = Backbone.View.extend({
	initialize: function() {
		this.listenTo(ceremonies, 'sync', this.get_citation_quote);
		this.listenTo(ceremonies, 'sync', this.get_citation_text);
		this.listenTo(ceremonies, 'sync', this.get_remarks_quote);
		this.listenTo(ceremonies, 'sync', this.get_remarks_text);
		this.listenTo(ceremonies, 'sync', this.init_heroes_quote);
		ceremonies.fetch();
		masthead.init();
	},
	get_citation_quote: function() {
		var enabled = ceremonies.models[0].get('enabled');
		if(enabled){
			$('#biography').removeClass("hide");
		}
		var pull_quote = ceremonies.models[0].get('pull_quote');
		this.add_citation_quote(pull_quote);
	},
	get_citation_text: function() {
		var text = ceremonies.models[0].get('text');
		$("#official_cite_text").html(text[0]);
	},
	get_remarks_text: function() {
		var text = ceremonies.models[1].get('text');
		$("#white_house_text").html(text[0]);
	},
	get_remarks_quote: function() {
		var enabled = ceremonies.models[1].get('enabled');
		if(enabled){
			$('#index_quote').removeClass("hide");
		}
		var pull_quote = ceremonies.models[1].get('pull_quote');
		this.add_remarks_quote(pull_quote);
	},
	add_citation_quote: function(quote) {
		$("#official_citation_quote blockquote").html(quote);
	},
	add_remarks_quote: function(quote) {
		$("#white_house_quote blockquote").html(quote);
	},
	add_heroes_quote: function(index) {
		var enabled = ceremonies.models[2].get('enabled');
		if(enabled){
			$('#unit_history').removeClass("hide");
		}
		var heroes = ceremonies.models[2].get('heroes');
		var hero = heroes[index];
		$("#hero_quote blockquote").html(hero.pull_quote);
		$("#hero_text").html(hero.text[0]);
		$('#unit_history .button a').attr('id', index);
	},
	init_heroes_quote: function() {
		this.add_heroes_quote(0);
	}
});

var UnitLinks = Backbone.View.extend({
	initialize: function() {},
	el: '.unit_nav ul a',
	events: {
		"click": "click"
	},
	click: function(e) {
		e.preventDefault();
		this.$el.removeClass('active');
		var index = this.$el.index(e.target);
		this.$el.eq(index).addClass('active');
		app.add_heroes_quote(index);
	}
});


var HeroButton = Backbone.View.extend({
	initialize: function() {},
	el: '#unit_history .button a',
	events: {
		"click": "click"
	},
	click: function(e) {
		e.preventDefault();
		lightbox.print_lb_html();
		var id = $(e.currentTarget).attr("id");
		var heroes = ceremonies.models[2].get('heroes');
		var hero = heroes[id];

		var content = $("#lightbox_content");
		content.append($("<h2>").prop({
			"class": 'center'
		}).html(hero.name));
		if (hero.text) {
			for (var i = 0; i < hero.text.length; i++) {
				content.append($("<p>").html(hero.text[i]));
			}
		}
	}
});

var RemarkButton = Backbone.View.extend({
	initialize: function() {},
	el: '#index_quote .button a',
	events: {
		"click": "click"
	},
	click: function(e) {
		e.preventDefault();
		lightbox.document_id = "document_presidential_remarks";
		lightbox.print_lb_html();
		var remarks = ceremonies.models[1];

		var content = $("#lightbox_content");
		content.append($("<h2>").prop({
			"class": 'center'
		}).html(remarks.get('doc_title')));
		
		var text = remarks.get('text');
		if (text) {
			for (var i = 0; i < text.length; i++) {
				content.append($("<p>").html(text[i]));
			}
		}
	}
});

var CitationButton = Backbone.View.extend({
	initialize: function() {},
	el: '#biography .button a',
	events: {
		"click": "click"
	},
	click: function(e) {
		e.preventDefault();
		lightbox.document_id = "document_official_citation";
		lightbox.print_lb_html();
		var citation = ceremonies.models[0];

		var content = $("#lightbox_content");
		content.append($("<h2>").prop({
			"class": 'center'
		}).html(citation.get('doc_title')));

		var text = citation.get('text');
		if (text) {
			for (var i = 0; i < text.length; i++) {
				content.append($("<p>").html(text[i]));
			}
		}
	}
});


var ceremonies = new CeremonyCollection();
var app = new ceremonyView();
var links = new UnitLinks();
var hero_button = new HeroButton();
var remark_button = new RemarkButton()
var citation_button = new CitationButton();
