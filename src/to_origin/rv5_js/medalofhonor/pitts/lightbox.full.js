/*global $,ga,Documents,app,Spinner,YT*/
var lightbox = {

	document_id: "",

	init: function(document_id) {
		lightbox.document_id = document_id;
		lightbox.print_lb_html();
		lightbox.add_content(document_id);

	},
	print_lb_html: function() {

		var lb = $("<div>").prop({
			"id": "lightbox"
		});

		lb.append(
			$("<div>").prop({
				"class": "container"
			}).append(
				$("<div>").prop({
					"id": "lightbox_outter_content_wrap"
				}).append(
					$("<div>").prop({
						"id": "lightbox_outter_content"
					}).append(
						$("<div>").prop({
							"class": "lightbox_button_wrap lightbox_button_wrap_right"
						}).append(
							$("<div>").prop({
								"class": "lightbox_buttons top_lightbox_buttons"
							}).append(
								$("<div>").prop({
									"class": "lightbox_button"
								}).click(
									function() {
										lightbox.close_lightbox();
									}
								).append(
									$("<div>").prop({
										"class": "button_icon",
										"id": "button_icon_close"
									})
								)
							)
						),

						$("<div>").prop({
							"id": "lightbox_inner_content_wrap"
						}).append(
							$("<div>").prop({
								"id": "lightbox_inner_content"
							}).append(
								$("<div>").prop({
									"id": "lightbox_content"
								})
							)
						)
					)
				)
			)
		);

		$("html").css("overflow-y", "hidden");

		$("#page").append(lb);

		lb.click(function() {
			lightbox.close_lightbox();
		});

		$("#lightbox_outter_content").click(function(e) {
			if (e.target.tagName != "A" || e.target.className != "download") {
				e.stopPropagation();
			}
		});

		$("#lightbox").focus();
		if (typeof ga === "function") {
			ga('send', 'event', 'lightbox', 'open', lightbox.document_id);
		}
		//lightbox.add_content("id");

	},

	add_content: function(document_id) {
		if (document_id.match(/document_/g)) {
			lightbox.show_document(document_id);
		} else if (document_id.match(/image_/g)) {
			lightbox.show_image(document_id, true);
		} else if (document_id.match(/video_/g)) {
			lightbox.show_video(document_id, true);
		}
	},

	close_lightbox: function() {
		$("#lightbox").remove();
		$("html").css("overflow-y", "scroll");
		if (typeof ga === "function") {
			ga('send', 'event', 'lightbox', 'close', lightbox.document_id);
		}
	},

	show_document: function(document_id) {

		if (document_id == "document_173rd_airborne" || document_id == "document_2nd_battalion" || document_id == "document_narrative") {
			var moh_document = new Documents();
			moh_document.onDocumentsObjectReady = function() {
				var moh_docs = moh_document.documents;
				var doc_index = 0;
				if (document_id == "document_173rd_airborne") {
					doc_index = 2;
				} else if (document_id == "document_2nd_battalion") {
					doc_index = 3;
				} else if (document_id == "document_narrative") {
					doc_index = 0;
				}
				var content = $("#lightbox_content");

				content.append(
					$("<h2>").html(moh_docs[doc_index].title)
				);

				var sections = moh_docs[doc_index].sections;
				lightbox.sections_count = 3;
				for (var i = 0; i < sections.length; i++) {
					lightbox.print_section(sections[i]);
					lightbox.sections_count = 3;
				}
			};
		}
	},

	show_video: function(video_id, autoplay) {
		var ytplayer, video_container, play = autoplay ? 1 : 0;

		ytplayer = $('<div>').prop({
			"id": "ytplayer"
		});

		video_container = $('<div>').prop({
			"class": "video-container"
		}).append(ytplayer);

		$("#lightbox_content").prepend(video_container);

		onYouTubePlayerAPIReady(video_id, play);
	},

	show_image: function(image_id, autoplay) {
		var image_path = "/e2/rv5_images/medalofhonor/pitts/multimedia/",
		content = $("#lightbox_content"),
		type = $(".multimedia_nav ul li a.active").prop("href"),
		images = app.collection.models,
		map = app.video_map(),
		download_link, bc_id, spinner, opts,
		current_img, first_img, last_img, next_img, prev_img;
		image_id = image_id.split("_").pop();
		type = type.split("#")[1];

		opts = {
			lines: 13, // The number of lines to draw
			length: 20, // The length of each line
			width: 10, // The line thickness
			radius: 30, // The radius of the inner circle
			corners: 1, // Corner roundness (0..1)
			rotate: 0, // The rotation offset
			direction: 1, // 1: clockwise, -1: counterclockwise
			color: '#000', // #rgb or #rrggbb or array of colors
			speed: 1, // Rounds per second
			trail: 60, // Afterglow percentage
			shadow: false, // Whether to render a shadow
			hwaccel: false, // Whether to use hardware acceleration
			className: 'spinner', // The CSS class to assign to the spinner
			zIndex: 2, // The z-index (defaults to 2000000000)
			top: '50%', // Top position relative to parent
			left: '50%' // Left position relative to parent
		};

		spinner = new Spinner(opts);

		if (image_id in map) {
			bc_id = map[image_id].bc_id;
		}

		current_img = $("#image_" + image_id);
		first_img = $(".media a:visible:first").attr("id").split("_").pop();
		last_img = $(".media a:visible:last").attr("id").split("_").pop();

		next_img = current_img.next(":visible").length ? current_img.next(":visible").attr("id").split("_").pop() : first_img;
		prev_img = current_img.prev(":visible").length ? current_img.prev(":visible").attr("id").split("_").pop() : last_img;

		$(".download").remove();

		if ($(".lightbox_image").length) {
			// replace image/video and caption and etc...
			$(".lightbox_image h2").html(images[image_id].get("title"));
			$(".lightbox_image p:first").html(images[image_id].get("caption"));

			if (bc_id) {
				$("#img_container,.video-container").remove();
				lightbox.show_video(bc_id, autoplay);
			} else {
				if (images[image_id].get("author")) {
					$('.lightbox_image p:first').append(
						" Photo Credit: " + images[image_id].get("author")
					);
				}
				download_link = $("<p>").prop({
						"class": "download"
					}).append(
						$("<a>").prop({
							"href": images[image_id].get("image"),
							"target": "_blank"
						}).append(
							"Download Image",
							$("<span>")
						).click(function() {
							if (typeof ga === "function") {
								ga('send', 'event', 'downloads', 'jpg', $(this).attr("href"));
							}
						})
					);

				$(".lightbox_image").append(download_link);
				$(".video-container").remove();
				if (!$(".lightbox_image #img_container img").length) {
					$(".lightbox_image").prepend(
						$("<div>").prop({"id":"img_container"}).append(
							$("<img>")
						)
					);
				}
				if (!$(".lightbox_image").find('.spinner').length) {
					spinner.spin($("#img_container")[0]);
				}
				$('<img>').load(function() {
					$(".lightbox_image img").prop({
						"src": image_path + images[image_id].get("id") + "/641.jpg",
						"alt": images[image_id].get("caption")
					});
					spinner.stop();
				}).attr('src', image_path + images[image_id].get("id") + "/641.jpg");
			
				$(".lightbox_image .download a").prop({
					"href": images[image_id].get("image")
				}).text("Download Image").append(
					$("<span>")
				);
			}
			$(".lightbox_button_wrap_right .bottom_lightbox_buttons .lightbox_button").off("click").on("click", function() {
				lightbox.show_image("image_" + next_img, false);
			});
			$(".lightbox_button_wrap_left .bottom_lightbox_buttons .lightbox_button").off("click").on("click", function() {
				lightbox.show_image("image_" + prev_img, false);
			});
		} else {
			content.append(
				$("<div>").prop({
					"class": "lightbox_image"
				}).append(
					$("<h2>").html(images[image_id].get("title")),
					$("<p>").html(images[image_id].get("caption"))
				)
			);

			if (bc_id) {
				lightbox.show_video(bc_id, autoplay);
			} else {
				if (images[image_id].get("author")) {
					$('.lightbox_image p:first').append(
						" Photo Credit: " + images[image_id].get("author")
					);
				}
				download_link = $("<p>").prop({
					"class": "download"
				}).append(
					$("<a>").prop({
						"href": images[image_id].get("image"),
						"target": "_blank"
					}).append(
						"Download Image",
						$("<span>")
					).click(function() {
						if (typeof ga === "function") {
							ga('send', 'event', 'downloads', 'jpg', $(this).attr("href"));
						}
					})
				);
					
				$(".lightbox_image").append(download_link);
				$(".lightbox_image").prepend($("<div>").prop({"id":"img_container"}).height(120));
				if (!$(".lightbox_image").find('.spinner').length) {
					spinner.spin($("#img_container")[0]);
				}
				$('<img>').load(function() {
					$("#img_container").height('').prepend(
						$("<img>").prop({
							"src": image_path + images[image_id].get("id") + "/641.jpg",
							"alt": images[image_id].get("caption")
						})
					);
					spinner.stop();
				}).attr('src', image_path + images[image_id].get("id") + "/641.jpg");
			}
			lightbox.show_prev_next(next_img, prev_img);
		}
	},

	show_prev_next: function(next_item, prev_item) {
		var lightbox_content_wrap = $("#lightbox_outter_content");
		lightbox_content_wrap.append(
			$("<div>").prop({
				"class": "lightbox_button_wrap lightbox_button_wrap_right"
			}).append(
				$("<div>").prop({
					"class": "lightbox_buttons bottom_lightbox_buttons"
				}).append(
					$("<div>").prop({
						"class": "lightbox_button"
					}).click(
						function() {
							lightbox.show_image("image_" + next_item, false);
						}
					).append(
						$("<div>").prop({
							"class": "button_icon",
							"id": "button_icon_next"
						})
					)
				)
			)
		);
		lightbox_content_wrap.append(
			$("<div>").prop({
				"class": "lightbox_button_wrap lightbox_button_wrap_left"
			}).append(
				$("<div>").prop({
					"class": "lightbox_buttons bottom_lightbox_buttons"
				}).append(
					$("<div>").prop({
						"class": "lightbox_button"
					}).click(
						function() {
							lightbox.show_image("image_" + prev_item, false);
						}
					).append(
						$("<div>").prop({
							"class": "button_icon",
							"id": "button_icon_prev"
						})
					)
				)
			)
		);
	},

	section_count: 3,

	print_section: function(section) {
		var content, i = 0, j = 0, k = 0,
		has_title = ("title" in section);
		content = $("#lightbox_content");
		if (section.title == "Unit History" || !has_title) {
			if (has_title) {
				content.append($("<h3>").html(section.title));
			}

			for (i; i < section.text.length; i++) {
				content.append(
					$("<p>").html(section.text[i])
				);
			}
		} else if (section.title == "Unit Insignia") {
			content.append($("<h3>").html(section.title));

			for (j; j < section.sections.length; j++) {
				content.append("<div class=\"unit_insignia\"></div>");
				content = $("#lightbox_content .unit_insignia:last");

				content.append($("<img>").prop({
					"src": section.sections[j].image
				}));

				content.append($("<h4>").html(section.sections[j].title));

				for (k; k < section.sections[j].sections.length; k++) {
					content.append(
						$("<h5>").html(section.sections[j].sections[k].title)
					);

					content.append(
						$("<p>").html(section.sections[j].sections[k].text)
					);
				}
				content = $("#lightbox_content");
			}
		}
	}

};


var player;
function onYouTubePlayerAPIReady(id,autoplay) {
	player = new YT.Player('ytplayer', {
		height: '390',
		width: '640',
		origin: 'https://www.army.mil/',
		videoId: id,
		playerVars: {
			wmode: 'opaque',
			autoplay: autoplay
		}
	});
}