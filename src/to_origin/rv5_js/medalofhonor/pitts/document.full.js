//Last Updated 2014-07-16 1650 ASB
//var DocumentModel = Backbone.Model.extend({
//	urlRoot: base_url + 'white/json/document.json'
//});
//
//var DocumentCollection = Backbone.Collection.extend({
//	model: DocumentModel,
//	url: base_url + 'white/json/document.json'
//});

function Documents() {
	var base_url = "https://www.army.mil/medalofhonor/";
	//var base_url = "http://localhost:8888/ardev/frontend/trunk/httpdocs/_development/";
	var context = this;
	
	this.documents = "";
	
	$.ajax({
		url: base_url + 'pitts/json/document.json',
		type: "GET",
		dataType: "json",
		success: function(data){
			if (data !== null) {
				context.documents = data;
			}
		},
		complete: function() {
			context.onDocumentsObjectReady(context);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			throw errorThrown;
		}
	});
}

Documents.prototype.onDocumentsObjectReady = function(context) {
	throw "The onDocumentsObjectReady(context) function must be inmplemented";
};