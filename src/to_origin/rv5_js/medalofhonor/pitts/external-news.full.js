//Last Updated 2014-07-16 1650 ASB
var ExternalArticle = Backbone.Model.extend({
	urlRoot: '//www.army.mil/medalofhonor/pitts/json/external_news.json'
});

var ExternalArticles = Backbone.Collection.extend({
	model: ExternalArticle,
	url: '//www.army.mil/medalofhonor/pitts/json/external_news.json'
});

var ext_articles = new ExternalArticles();

var ExternalArticleGallery = Backbone.View.extend({
	initialize: function() {
		this.listenTo(ext_articles, 'sync', this.load_articles);
		ext_articles.fetch();
	},
	load_articles: function(){
		var section  = $("#ext_news_section");
		for(var i = 0; i < ext_articles.length; i++){
			var article_source = ext_articles.models[i].get("article_source");
			var article_title = ext_articles.models[i].get("article_title");
			var article_url  = ext_articles.models[i].get("article_url");
			
			var article_source_str = "<span>"+article_source+"</span>: ";
			
			var link = $('<a>').prop({"href":article_url,"target": "_blank" }).html(article_source_str+article_title).click(function(){
				if (typeof ga === "function") {
					ga('send', 'event', 'outbound', 'click', $(this).attr("href"));
				}
			});
			section.append(link);
		}
	}
});
