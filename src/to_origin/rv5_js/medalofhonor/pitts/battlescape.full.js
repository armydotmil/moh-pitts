//Last Updated 2014-07-21 0927 ASB
//
//
// var Hero = Backbone.Model.extend();
//
// var Fallen_Heros = Backbone.Collection.extend({
// 	model: Hero,
// 	url: 'http://localhost:8888/ardev/frontend/trunk/httpdocs/_development/pitts/json/fallen.json'
// });
//
//
// var Fallen_Cell = Backbone.View.extend({
// 	el: $('#fallen_row'),
//
// 	initialize: function(){
// 		_.bindAll(this, 'render');
//
// 		this.collection = new Fallen_Heros;
// 	}
// });

var Callout = Backbone.Model.extend({
	urlRoot: '//www.army.mil/medalofhonor/pitts/json/battlescape.json'
});

var Callouts = Backbone.Collection.extend({
	model: Callout,
	url: '//www.army.mil/medalofhonor/pitts/json/battlescape.json'
});

var CalloutView = Backbone.View.extend({
	callouts: "",
	initialize: function() {
		this.callouts = new Callouts();
		this.listenTo(this.callouts, 'sync', this.display);
		this.listenTo(this.callouts, 'sync', this.displayAudio);
		this.callouts.fetch();
	},
	display: function() {
		var holders = $(".callout_wrap");
		var callouts = this.callouts.models[0].get('callouts');
		for (var i = 0; i < holders.length; i++) {
			callout_index = i + 1;
			var type = callouts[i].type;
			var image_div = $("<a>").prop({
				"class": "callout_" + type + "_wrap callout_content " + type + ""
			}).click(function() {
				lightbox.print_lb_html();
				var index = $(".callout_content").index($(this));
				var image_index = index + 1;
				var caption = callouts[index].caption;
				var title = callouts[index].title;
				if ($(this).hasClass('image')) {
					var lightbox_image = $("<img>").prop({
						"src": "/e2/rv5_images/medalofhonor/pitts/battle/callouts/" + image_index + "/641.jpg",
						"alt": caption
					});

					var h2 = $('<h2>').html(title);

					var p = $('<p>').html(caption);

					$("#lightbox_content").append(lightbox_image).append(h2).append(p);

				} else if ($(this).hasClass('video')) {
					
					lightbox.show_video(callouts[index].video_id,0);

					var h2 = $('<h2>').html(title);

					var p = $('<p>').html(caption);
					
					$("#lightbox_content").append(h2).append(p);
					// Replace the 'ytplayer' element with an <iframe> and
					// YouTube player after the API code downloads.
									
				}//
			});

			var callout_size = get_callout_img(i);
			
			image_div.append(
				$("<img>").prop({
					"alt": " ",
					"src": "/e2/rv5_images/medalofhonor/pitts/battle/callouts/" + callout_index + "/" + callout_size
				})
			);
			image_div.append($("<div>").prop({
				'class': 'icon_overlay'
			}));

			$(holders[i]).append(image_div);


			$(holders[i]).append(
				$("<p>").html(callouts[i].title)
			);
			
			/*
console.log(i);
			
			if( i == 1 ){
				holders[i].append(
					$('<p>').prop({
						'class' : 'download'
					}).append(
						$("<a>").prop({
							'href' : "/e2/rv5_downloads/medalofhonor/pitts/video/kahler_terrain.mp4",
							'target' : '_blank'
						}).html("Download Video (69 MB)<span></span>")
					)
				);
			}
*/
		}
	},
	displayAudio: function() {
		var audio = this.callouts.models[0].get('audio');
		var holders = $('.audio_player_container');
		audio_player.add_player(audio[0]);

		for (var i = 0; i < holders.length; i++) {
			var link = $("<a>").prop({
				"class" : "transcript"
			}).text("Read audio transcript");

			var div = $("<div>").prop({
				"class" : "audio_player_wrap"
			});

			var wrap1 = $("<div>").prop({
				"class" : "inner_wrap"
			}).append(
				$("<div>").prop({
					"class" : "audio_thumbnail"
				})
			).append(link);

			var wrap2 = $("<div>").prop({
				"class" : "inner_wrap_2"
			}).append(
				$("<h3>").text('"' + audio[i].quote + '"')
			).append(
				audio_player.print_html(audio[i])
			).append(link.clone().addClass('mobile_transcript'));
			
			div.append(wrap1).append(wrap2);

			$(holders[i]).append(div);
		}
		$('.transcript').click(function(e) {
			e.preventDefault();
			var index = ($(this).hasClass('mobile_transcript')) ? $('.mobile_transcript').index($(this)) : $('.inner_wrap .transcript').index($(this));
			lightbox.print_lb_html();
			
			$("#lightbox_content").append(
				$("<h3>").text(audio[index].title)
			);
			for (var i = 0; i < audio[index].transcript.length; i++) {
				var transcript = audio[index].transcript[i];
				$("#lightbox_content").append(
					$("<p>").text(transcript)
				);
			}
		});
		audio_player.bind_events();
	}
});

var player;
function onYouTubePlayerAPIReady(id) {
	player = new YT.Player('ytplayer', {
		height: '390',
		width: '640',
		origin: 'https://www.army.mil/',
		videoId: id,
		playerVars: {
			wmode: 'opaque'
		}
	});
}

function build_fallen_grid() {
	for (var i = 0; i < fallen_data.length; i++) {
		var image_size = get_fallen_img();
		$("#fallen_row").append(
			$("<div>").prop({
				"class": "fallen_cell_wrap"
			}).append(
				$("<div>").prop({
					"class": "fallen_cell"
				}).append(
					$("<img>").prop({
						"alt": fallen_data[i].name,
						"src": "/e2/rv5_images/medalofhonor/pitts/battle/fallen_heroes/" + fallen_data[i].id + "/"+image_size
					}),
					$("<div>").prop({
						"class": "fallen_overlay"
					}),
					$("<div>").prop({
						"class": "fallen_text"
					}).append(
						$("<h5>").text(fallen_data[i].name),
						$("<p>").prop({
							'class' : 'posthumous'
						}).append(fallen_data[i].promotion + "&nbsp;"),
						$("<div>").prop({
							"class" : "fallen_info"	
						}).append(
							$("<p class='fallen_age'>").append("<span>AGE</span> " + fallen_data[i].age),
							$("<p>").append("<span>HOMETOWN</span> " + fallen_data[i].hometown),
							$("<p>").append("<span>MILITARY OCCUPATION</span> " + fallen_data[i].mos)
						)
					)
				)
			)
		);
	}
	set_fallen_height();
}

function build_team_wall() {
	
	var mos_array = new Array();
	var team_text = $("#fallen_wall_text");
	
	for (var i = 0; i < team_data.length; i++) {
		if(mos_array.indexOf(team_data[i].mos) == -1) {
			mos_array.push(team_data[i].mos);
			//console.log(team_data[i].mos);
		}
				
		var mos_class = team_data[i].mos.replace(/ /g,"_").replace(/\./g, '_');
		
		$(team_text).append(
				$("<span>").prop({
					"class": 'soldier '+mos_class
				}).append(
					$("<span>").prop({
						"class": "team_rank"
					}).text(team_data[i].rank + " "),
					$("<span>").prop({
						"class": "name"
					}).text(team_data[i].first + " " + team_data[i].last)
				)
			);
			
			
			if( i != team_data.length - 1) {
				$(team_text).append("<span class='soldier_bullet'>&nbsp;&bull;&nbsp;</span>");				
			}
	}
	mos_array.sort();
	
	//console.log(mos_array);

	for(var i = 0; i < mos_array.length; i++) {
		//console.log(mos_array[i]);
		$("#team_controls ul").append(
			$("<li>").append(
				$("<a>").prop({
					"href" : "#",
					"id" : mos_array[i].replace(/ /g,"_").replace(/\./g, '_')
				}).text(mos_array[i]).hover(function(){
					$("span.soldier, span.soldier_bullet").css('color','#9d9b95');
					$("."+$(this).attr("id").replace(/ /g,"_").replace(/\./g, '_')).css('color',"#343330");
				}, function(){
					$("span.soldier, span.soldier_bullet").css('color', '#343330');
				}).click(function(){return false;})
			)
		);
	}
}

function build_callout_image(callout_index) {

	callout_index = callout_index + 1;

	var image_div = $("<a>").prop({
		"class": "callout_image_wrap",
		"href": "javascript:;"
	})

	//var callout_size = get_callout_img();

	image_div.append(
		$("<img>").prop({
			"src": "/e2/rv5_images/medalofhonor/pitts/battle/callouts/" + callout_index + "/" + callout_size
		})
	)

	return image_div;
}

function build_callout_video(callout_index) {
	callout_index = callout_index + 1;
	var video_div = $("<a>").prop({
		"class": "callout_video_wrap",
		"href": "javascript:;"
	})

	video_div.append(
		$("<img>").prop({
			"src": "/e2/rv5_images/medalofhonor/pitts/battle/callouts/" + callout_index + "/" + callout_size
		})
	);

	return video_div;
}

var opts = {
	lines: 13, // The number of lines to draw
	length: 20, // The length of each line
	width: 10, // The line thickness
	radius: 30, // The radius of the inner circle
	corners: 1, // Corner roundness (0..1)
	rotate: 0, // The rotation offset
	direction: 1, // 1: clockwise, -1: counterclockwise
	color: '#000', // #rgb or #rrggbb or array of colors
	speed: 1, // Rounds per second
	trail: 60, // Afterglow percentage
	shadow: false, // Whether to render a shadow
	hwaccel: false, // Whether to use hardware acceleration
	className: 'spinner', // The CSS class to assign to the spinner
	zIndex: 2, // The z-index (defaults to 2000000000)
	top: '50%', // Top position relative to parent
	left: '50%' // Left position relative to parent
};

var spinner = new Spinner(opts);

var img_path = "/e2/rv5_images/medalofhonor/pitts/battle/team_slideshow/";

function send_download_event(type, item_href) {
	if (typeof ga === "function") {
		ga('send', 'event', 'downloads', type, item_href);
	}
}

function build_slideshow() {
	var img_size = get_slideshow_img();
	for (var i = 0; i < slideshow_data.length; i++) {
		if (slideshow_data[i].folder && slideshow_data[i].caption) {

			$("#team_slideshow .img_container").append(
				$("<img/>").attr({ // append img tags without the src -- only set the src when user clicks next or prev
					"id" : i
				}).hide()
			);
			
			if (i === 0) {
				var img = img_path + slideshow_data[i].folder + "/" + img_size;
				
				spinner.spin($("#team_slideshow .img_container")[0]);
				$('<img>').load(function() {
					$("#team_slideshow .img_container img:first").attr("src", img);
					spinner.stop();
				}).attr('src', img);
			}
			$(".img_container img:first").show().addClass("current");
		}
	}
	$("#team_slideshow p:first").text(slideshow_data[0].caption);
	$("#team_slideshow .download a").prop({
		"href" : img_path + slideshow_data[0].folder + "/original.jpg",
		"target" : "_blank"
	}).click(function(){
		send_download_event('jpg', img_path + $(this).attr('href'));
	});
	$("#team_slideshow span:last").text("1 of " + slideshow_data.length);
}

function change_slide(direction) {
	var current = $(".img_container img.current");
	var next = $(".img_container img.current").next('img');
	var prev = $(".img_container img.current").prev('img');
	var disp_obj, id, img;
	var img_size = get_slideshow_img();
	
	if (!prev.length) {
		prev = $(".img_container img:last");
	}
	
	if (!next.length) {
		next = $(".img_container img:first");
	}
	
	disp_obj = (direction.match("next")) ? next : prev;
	id = disp_obj.attr('id');
	img = img_path + slideshow_data[id].folder + "/" + img_size;
	
	current.fadeOut(function() {
		
		spinner.spin($("#team_slideshow .img_container")[0]);
		$('<img>').load(function() {
			disp_obj.attr("src", img);
			spinner.stop();
		}).attr('src', img);
		
		disp_obj.fadeIn().addClass("current");
		
		current.removeClass();
	
		$("#team_slideshow p:first").text(slideshow_data[id].caption);
		$("#team_slideshow .download a").prop({
			"href" : img_path + slideshow_data[id].folder + "/original.jpg",
			"target" : "_blank"
		});
		id = parseInt(id,10) + 1;
		$("#team_slideshow span:last").text(id + " of " + slideshow_data.length);
	});
}

function get_slideshow_img() {
	var width = $(window).width();
	
	if (width > 768) {
		return "484.jpg";
	} else if (width > 480 && width <= 768) {
		return "476.jpg"
	} else if (width <= 480) {
		return "221.jpg"
	}
}

function get_callout_img(i) {
	var index = i + 1;
	var width = $(window).width();
	
	if (width > 768) {
		return "295.jpg";
	} else if(width <= 768 && width > 480){	
		if( index < 3 ){
			return "359.jpg";
		} else if( index == 3 || index == 7) {
			return "608.jpg";
		} else if( index > 3 && index < 7 ){
			return "234.jpg";
		}
	} else if (width <= 480) {
		return "451.jpg";
	}
}

function get_fallen_img() {
	var width = $(window).width();
	
	if (width > 768) {
		return "311.jpg";
	} else if (width > 480 && width <= 768) {
		return "359.jpg"
	} else if (width <= 480) {
		return "451.jpg"
	}
}

function set_fallen_height() {
	//Resize fallen hero grid
	
	var width = $(window).width();
	var fallen_cells = $(".fallen_cell");
	
	if(width <= 768 && width > 480) {
		for(var i = 0; i < fallen_cells.length; i++) {
			if(i % 2 != 0) {
				if($(fallen_cells[i]).height() < $(fallen_cells[i-1]).height()) {
					$(fallen_cells[i]).height($(fallen_cells[i-1]).height());
				}
			}
		}
	} else {
		for(var i = 0; i < fallen_cells.length; i++) {
			$(fallen_cells[i]).height('');
		}
	} 

	
	
}

$(document).ready(function() {
	if(!Array.prototype.indexOf) {
	    Array.prototype.indexOf = function(obj, start) {
	         for (var i = (start || 0), j = this.length; i < j; i++) {
	             if (this[i] === obj) { return i; }
	         }
	         return -1;
	    }
	}
	var tag = document.createElement('script');
	tag.src = "https://www.youtube.com/player_api";
	var firstScriptTag = document.getElementsByTagName('script')[0];
	firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

	masthead.init();
	//var fallen = new Fallen_Heros;

	audio_player.detect_flash();

	$("#document_narrative").click(function() {
		lightbox.init("document_narrative");
	});

	build_fallen_grid();
	build_team_wall();
	//build_callouts();
	build_slideshow();

	$(".slideshow_button").on('click', function(e) {
		var direction = $(e.currentTarget).find('div').attr('id');
		change_slide(direction);
	});	

	var callout = new CalloutView();
	
	var infographic_src = "";
	
	if(Modernizr.svg){
		infographic_src = '/e2/rv5_images/medalofhonor/pitts/battle/infographic/infographic.svg';
	} else {
		infographic_src = '/e2/rv5_images/medalofhonor/pitts/battle/infographic/infographic.png';
	}
	
	$('#infographic_img').prop('src', infographic_src);
	
	$(".full_video p.download a").click(function(){
		send_download_event('mp4', $(this).attr('href'));
	});

});

$(window).resize(function() {
	// only replacing one image in this case - the rest will be loaded properly via change_slide function
	var current = $(".img_container .current");
	var new_size = get_slideshow_img();
	
	if (current.length) {
		var src = current.prop('src').replace(/\d\d\d\.jpg/g, new_size);
		current.prop('src', src);
	}
	
	//Change sizes of fallen hero images
	var fallen_imgs = $(".fallen_cell img");
	//console.log(fallen_imgs);
	var new_size = get_fallen_img();
	for(var i = 0; i < fallen_imgs.length; i++) {
		var src = $(fallen_imgs[i]).prop('src').replace(/\d\d\d\.jpg/g, new_size);
		//console.log(src);
		$(fallen_imgs[i]).prop('src', src);
	}
	
	set_fallen_height();
	
});

var fallen_data = [{
	"id": "abad",
	"name": "Spc. Sergio S. Abad",
	"promotion": "Posthumously promoted from private first class",
	"age": "21",
	"hometown": "Morganfield, Ky.",
	"mos": "Indirect Fire Infantryman"
}, {
	"id": "ayers",
	"name": "Cpl. Jonathan R. Ayers",
	"promotion": "Posthumously promoted from specialist",
	"age": "24",
	"hometown": "Snellville, Ga.",
	"mos": "Infantryman"
}, {
	"id": "bogar",
	"name": "Cpl. Jason M. Bogar",
	"promotion": "Posthumously promoted from specialist",
	"age": "25",
	"hometown": "Seattle, Wash.",
	"mos": "Infantryman"
}, {
	"id": "brostrom",
	"name": "1st Lt. Jonathan P. Brostrom",
	"promotion": "",
	"age": "24",
	"hometown": "Hawaii",
	"mos": "Infantry Officer"
}, {
	"id": "garcia",
	"name": "Sgt. Israel Garcia",
	"promotion": "",
	"age": "24",
	"hometown": "Long Beach, Calif.",
	"mos": "Infantryman"
}, {
	"id": "hovater",
	"name": "Cpl. Jason D. Hovater",
	"promotion": "Posthumously promoted from specialist",
	"age": "24",
	"hometown": "Clinton, Tenn.",
	"mos": "Infantryman"
}, {
	"id": "phillips",
	"name": "Cpl. Matthew B. Phillips",
	"promotion": "Posthumously promoted from specialist",
	"age": "27",
	"hometown": "Jasper, Ga.",
	"mos": "Infantryman"
}, {
	"id": "rainey",
	"name": "Cpl. Pruitt A. Rainey",
	"promotion": "Posthumously promoted from specialist",
	"age": "22",
	"hometown": "Haw River, N.C.",
	"mos": "Infantryman"
}, {
	"id": "zwilling",
	"name": "Cpl. Gunnar W. Zwilling",
	"promotion": "Posthumously promoted from specialist",
	"age": "20",
	"hometown": "Florissant, Mo.",
	"mos": "Infantryman"
}];

var team_data = [
  {
    "rank":"Sgt.",
    "first":"Eric",
    "last":"Aass",
    "mos":"Infantrymen"
  },
  {
    "rank":"1st Sgt.",
    "first":"Scott",
    "last":"Beeson",
    "mos":"Infantry Senior Sergeants"
  },
  {
    "rank":"Staff Sgt.",
    "first":"Jonathan",
    "last":"Benton",
    "mos":"Infantrymen"
  },
  {
    "rank":"Spc.",
    "first":"Shane",
    "last":"Burton",
    "mos":"Infantrymen"
  },
  {
    "rank":"Spc.",
    "first":"Jack ",
    "last":"Butterfield",
    "mos":"Combat Engineers"
  },
  {
    "rank":"Chief Warrant Officer 4",
    "first":"Joseph Nowell",
    "last":"Callaway",
    "mos":"UH-60 MedEvac Aircrews"
  },
  {
    "rank":"Sgt.",
    "first":"Hector",
    "last":"Chavez",
    "mos":"Infantrymen"
  },
  {
    "rank":"Cpl.",
    "first":"Derek ",
    "last":"Christophersen",
    "mos":"Combat Engineers"
  },
  {
    "rank":"Chief Warrant Officer 3",
    "first":"Eric William",
    "last":"Collier",
    "mos":"UH-60 MedEvac Aircrews"
  },
  {
    "rank":"Chief Warrant Officer 2",
    "first":"Nicholas Thor",
    "last":"Dance",
    "mos":"UH-60 MedEvac Aircrews"
  },
  {
    "rank":"Spc.",
    "first":"Aaron",
    "last":"Davis",
    "mos":"Infantrymen"
  },
  {
    "rank":"Staff Sgt.",
    "first":"Adam",
    "last":"Delaney",
    "mos":"Infantrymen"
  },
  {
    "rank":"Chief Warrant Officer 2",
    "first":"Jeremy Paul",
    "last":"Delk",
    "mos":"UH-60 MedEvac Aircrews"
  },
  {
    "rank":"Staff Sgt.",
    "first":"",
    "last":"Dency",
    "mos":"UH-60 MedEvac Aircrews"
  },
  {
    "rank":"Spc.",
    "first":"Michael",
    "last":"Denton",
    "mos":"Infantrymen"
  },
  {
    "rank":"Sgt. 1st Class",
    "first":"David L.",
    "last":"Dzwik",
    "mos":"Infantry Senior Sergeants"
  },
  {
    "rank":"Sgt.",
    "first":"",
    "last":"Frailey",
    "mos":"UH-60 MedEvac Aircrews"
  },
  {
    "rank":"Chief Warrant Officer 3",
    "first":"Jonny",
    "last":"Gavreau",
    "mos":"AH-64D Apache Aircrews"
  },
  {
    "rank":"1st Lt.",
    "first":"Devin",
    "last":"George",
    "mos":"Infantry Officers"
  },
  {
    "rank":"Sgt.",
    "first":"Jared",
    "last":"Gilmore",
    "mos":"Indirect Fire Infantrymen"
  },
  {
    "rank":"Sgt.",
    "first":"Matthew",
    "last":"Gobble",
    "mos":"Infantrymen"
  },
  {
    "rank":"Staff Sgt.",
    "first":"Lucas",
    "last":"Gonzales",
    "mos":"Infantrymen"
  },
  {
    "rank":"2nd Lt.",
    "first":"Eric",
    "last":"Gonzalez",
    "mos":"Field Artillery Officer"
  },
  {
    "rank":"Spc.",
    "first":"Reid",
    "last":"Grapes",
    "mos":"Infantrymen"
  },
  {
    "rank":"Staff Sgt.",
    "first":"Justin",
    "last":"Grimm",
    "mos":"Infantrymen"
  },
  {
    "rank":"Chief Warrant Officer 2",
    "first":"Juan Luis",
    "last":"Guzman Jr.",
    "mos":"UH-60 MedEvac Aircrews"
  },
  {
    "rank":"Spc.",
    "first":"Adam",
    "last":"Hamby",
    "mos":"Infantrymen"
  },
  {
    "rank":"Spc.",
    "first":"Tyler",
    "last":"Hanson",
    "mos":"Infantrymen"
  },
  {
    "rank":"Sgt.",
    "first":"John",
    "last":"Hayes",
    "mos":"Infantrymen"
  },
  {
    "rank":"Staff Sgt.",
    "first":"William Richard",
    "last":"Helfrich",
    "mos":"UH-60 MedEvac Aircrews"
  },
  {
    "rank":"Spc.",
    "first":"William",
    "last":"Hewitt",
    "mos":"Combat Medics"
  },
  {
    "rank":"Chief Warrant Officer 3",
    "first":"Christopher Mikel",
    "last":"Hill",
    "mos":"UH-60 MedEvac Aircrews"
  },
  {
    "rank":"Sgt.",
    "first":"Brian",
    "last":"Hissong",
    "mos":"Infantrymen"
  },
  {
    "rank":"Staff Sgt.",
    "first":"Thomas ",
    "last":"Hodge",
    "mos":"Combat Engineers"
  },
  {
    "rank":"Cpl.",
    "first":"Jason ",
    "last":"Jones",
    "mos":"U.S. Marine Corps Embedded Training Team"
  },
  {
    "rank":"Capt.",
    "first":"Kevin John",
    "last":"King",
    "mos":"Physician Assistant"
  },
  {
    "rank":"Staff Sgt.",
    "first":"Matthew Sean",
    "last":"Kinney",
    "mos":"Combat Medics"
  },
  {
    "rank":"Pfc.",
    "first":"William",
    "last":"Krupa",
    "mos":"Infantrymen"
  },
  {
    "rank":"Capt.",
    "first":"Justin Jeffrey",
    "last":"Madill",
    "mos":"Flight Surgeon"
  },
  {
    "rank":"Sgt.",
    "first":"Matthew",
    "last":"May",
    "mos":"Infantrymen"
  },
  {
    "rank":"Chief Warrant Officer 2",
    "first":"Wayne Antony",
    "last":"McDonald",
    "mos":"UH-60 MedEvac Aircrews"
  },
  {
    "rank":"Spc.",
    "first":"Chris",
    "last":"McKaig",
    "mos":"Combat Medics"
  },
  {
    "rank":"Sgt.",
    "first":"Dylan",
    "last":"Meyer",
    "mos":"Infantrymen"
  },
  {
    "rank":"Spc.",
    "first":"Jeffrey",
    "last":"Molnar",
    "mos":"Infantrymen"
  },
  {
    "rank":"Sgt. Maj.",
    "first":"",
    "last":"Morales",
    "mos":"UH-60 MedEvac Aircrews"
  },
  {
    "rank":"Chief Warrant Officer 3",
    "first":"James (Jimmy) Edward",
    "last":"Morrow III",
    "mos":"AH-64D Apache Aircrews"
  },
  {
    "rank":"Capt.",
    "first":"Matthew",
    "last":"Myer",
    "mos":"Infantry Officers"
  },
  {
    "rank":"Spc.",
    "first":"Ananthachai",
    "last":"Nantakul",
    "mos":"Infantrymen"
  },
  {
    "rank":"Cpl.",
    "first":"Jason ",
    "last":"Oakes",
    "mos":"U.S. Marine Corps Embedded Training Team"
  },
  {
    "rank":"Staff Sgt.",
    "first":"Erich",
    "last":"Phillips",
    "mos":"Infantrymen"
  },
  {
    "rank":"Staff Sgt.",
    "first":"Jesse",
    "last":"Queck",
    "mos":"Indirect Fire Infantrymen"
  },
  {
    "rank":"Sgt.",
    "first":"Luis ",
    "last":"Repreza",
    "mos":"U.S. Marine Corps Embedded Training Team"
  },
  {
    "rank":"Staff Sgt.",
    "first":"Sean",
    "last":"Samaroo",
    "mos":"Infantrymen"
  },
  {
    "rank":"Sgt.",
    "first":"Mike",
    "last":"Santiago",
    "mos":"Infantrymen"
  },
  {
    "rank":"Spc.",
    "first":"Jeffrey",
    "last":"Scantlin",
    "mos":"Infantrymen"
  },
  {
    "rank":"Spc.",
    "first":"James",
    "last":"Schmidt",
    "mos":"Infantrymen"
  },
  {
    "rank":"Capt.",
    "first":"Benjamin Alan",
    "last":"Seipel",
    "mos":"UH-60 MedEvac Aircrews"
  },
  {
    "rank":"Spc.",
    "first":"Nikhil",
    "last":"Shelke",
    "mos":"Combat Medics"
  },
  {
    "rank":"Staff Sgt.",
    "first":"Kyle",
    "last":"Silvernale",
    "mos":"Infantrymen"
  },
  {
    "rank":"Chief Warrant Officer 2",
    "first":"Isaac",
    "last":"Smith",
    "mos":"UH-60 MedEvac Aircrews"
  },
  {
    "rank":"Spc.",
    "first":"Jacob",
    "last":"Sones",
    "mos":"Infantrymen"
  },
  {
    "rank":"Spc.",
    "first":"Tyler",
    "last":"Stafford",
    "mos":"Infantrymen"
  },
  {
    "rank":"Pfc.",
    "first":"Scott",
    "last":"Stenoski",
    "mos":"Infantrymen"
  },
  {
    "rank":"Sgt. 1st Class",
    "first":"Shane",
    "last":"Stockard",
    "mos":"Infantry Senior Sergeants"
  },
  {
    "rank":"Spc.",
    "first":"Michael A.",
    "last":"Tellez",
    "mos":"Carpentry and Masonry Specialist"
  },
  {
    "rank":"Capt.",
    "first":"Walt",
    "last":"Tompkins",
    "mos":"Infantry Officers"
  },
  {
    "rank":"Staff Sgt.",
    "first":"Atwon",
    "last":"Thompkins",
    "mos":"Combat Medics"
  },
  {
    "rank":"1st Lt.",
    "first":"Aaron",
    "last":"Thurman",
    "mos":"Infantry Officers"
  },
  {
    "rank":"Chief Warrant Officer 3",
    "first":"Brian Jeffrey",
    "last":"Townsend",
    "mos":"AH-64D Apache Aircrews"
  },
  {
    "rank":"Sgt.",
    "first":"Jacob",
    "last":"Walker",
    "mos":"Infantrymen"
  },
  {
    "rank":"Chief Warrant Officer 2",
    "first":"Thieman Lee",
    "last":"Watkins Jr.",
    "mos":"AH-64D Apache Aircrews"
  }
];

var slideshow_data = [{
	"folder" : "01",
	"caption" : "(Left to right) Sgt. Matthew Gobble, Sgt. Ryan Pitts, then-Sgt. Adam Delaney, Sgt. Dylan Meyer, Sgt. Brian Hissong, Sgt. Mike Santiago and Sgt. Israel Garcia, with 2nd Platoon, Chosen Company, pause for a photo before going out on patrol, at Forward Operating Base Blessing, Nangalam, Afghanistan, spring/summer 2007."
},{
	"folder" : "02",
	"caption" : "(Left to right) Spc. Francisco Rodriguez Paco, Spc. Adam Hamby, Sgt. Israel Garcia and Pfc. William Krupa, with 3rd Squad, 2nd Platoon, Chosen Company, take a break from building a traffic control point northeast of Combat Outpost Bella, Afghanistan, spring 2008. The traffic control point was on the road from COP Bella to Aranas, Afghanistan."
},{
	"folder" : "03",
	"caption" : "(Left to right/top row) Spc. Francisco Rodriguez, Staff Sgt. John Otfinoski, Spc. Adam Hamby, Sgt. Mike Santiago, Spc. Jacob Sones, (left to right/bottom row) Spc. Tyler Stafford, Spc. James Schmidt, Sgt. \"Doc\" Wise, with 3rd Squad, 2nd Platoon, Chosen Company, pause for a photo af Observation Post Speedbump, near Combat Outpost Bella, Afghanistan, winter/spring 2008."
},{
	"folder" : "04",
	"caption" : "(Left to right/middle row) Cpl. Jason D. Hovater, then-Sgt. Adam Delaney, (unknown in back), Staff Sgt. Jonathan Benton, Spc. Tarien Lister, Spc. Jeffrey Scantlin, (left to right/front row) Spc. Reid Grapes, then Spc. Jacob Walker, with A Team, 2nd Squad, 2nd Platoon, Chosen Company. Date and location the photo was taken are unknown."
},{
	"folder" : "05",
	"caption" : "(Left to right) Cpl. Jonathan Ayers, Cpl. Jason M. Bogar, Spc. Nathan Wright, Spc. Chris McKaig and Sgt. Brian Hissong pause for a photo at Observation Post 1, near Combat Outpost Bella, Afghanistan, spring 2008."
},{
	"folder" : "06",
	"caption" : "Pictured here are Sgt. Nomack Valles, Spc. Kyle Fleming, Spc. Wesley Yost, who comprised the Combat Outpost Bella Mortar Team, Call Sign \"Thunder,\" at the COP Bella, Afghanistan, mortar pit, spring 2008."
},{
	"folder" : "08",
	"caption" : "Pictured here is Sgt. Dylan Meyer at Combat Outpost Bella, Afghanistan, spring 2008."
},{
	"folder" : "09",
	"caption" : "Pictured here is Pfc. William Krupa on patrol north of Observation Point Speedbump, near Combat Outpost Bella, Afghanistan, spring 2008."
},{
	"folder" : "10",
	"caption" : "Pictured here is Spc. Jeffrey Scantlin before a patrol, at Forward Operating Base Blessing, Nangalam, Afghanistan, spring 2008."
},{
	"folder" : "11",
	"caption" : "Pictured here, Cpl. Gunnar W. Zwilling pulls security while on patrol outside of Forward Operating Base Blessing, Nangalam, Afghanistan."
},{
	"folder" : "12",
	"caption" : "Sgt. Mike Santiago pulls security at Observation Post Speedbump, near Combat Outpost Bella, Afghanistan, spring 2008."
},{
	"folder" : "13",
	"caption" : "Pictured here is Sgt. 1st Class David L. Dzwik in Afghanistan, spring 2008."
},{
	"folder" : "14",
	"caption" : "Pictured here are Spc. Richard Ingledue (left) and Staff Sgt. Samaroo, while on patrol near Forward Operating Base Blessing, Nangalam, Afghanistan."
},{
	"folder" : "15",
	"caption" : "Pictured here are Spc. Tyler Stafford (front left), Cpl. Matthew B. Phillips (front right) and Spc. Gunnar Zwilling help build a traffic control point northeast of Combat Outpost Bella, Afghanistan, spring 2008. The road runs from COP Bella to Aranas."
},{
	"folder" : "16",
	"caption" : "In this undated photo, then-Capt. Matthew Myer takes a knee while on patrol in Afghanistan."
},{
	"folder" : "17",
	"caption" : "In this undated photo, Spc. Michael Denton is on patrol in Afghanistan."
},{
	"folder" : "18",
	"caption" : "(Left to right) Spc. William Hewitt, Cpl. Jonathan R. Ayers and Spc. Chris McKaig pull security at Observation Post 1, near Combat Outpost Bella, Afghanistan, spring 2008."
},{
	"folder" : "19",
	"caption" : "Pictured here is 2nd Platoon, Chosen Company, at Forward Operating Base Blessing, Nangalam, Afghanistan. Pictured are: (left to right/back row) Staff Sgt. Sam Miller, Pfc. Nick Murray, Cpl. Pruitt Rainey, Spc. Laquan Borden, Sgt. Mike Santiago, Spc. Ryan Schwartz, Spc. Tyler Stafford, Spc. John Hayes, Staff Sgt. Jonathan Benton, Cpl. Matthew Phillips, Spc. Adam Hamby, Spc. Reid Grapes, Spc. Tarien Lister, Spc. Jeff Molnar, Spc. Jacob Sones, Spc. Richard Ingledue, Spc. Tyler Hanson, Sgt. Ryan Pitts, Pfc. Joseph Latulippe, (left to right/front row) Sgt. 1st Class Matthew Kahler, Sgt. Brian Hissong, Sgt. Dylan Meyer, Staff Sgt. Lucas Gonzalez, Sgt. Adam Delaney, Sgt. Matthew Gobble, Sgt. Israel Garcia, Staff Sgt. Sean Samaroo, Spc. James Schmidt, Pfc. Ian Eads, Spc. Jacob Walker, Cpl. Gunnar Zwilling, Spc. Michael Denton, Spc. Nathan Wright, Cpl. Jason Hovater, Spc. Francisco Rodriquez, Spc. Joshua Sowells and 1st Lt. Devin George."
},{
	"folder" : "20",
	"caption" : "Pictured here are members of 1st Platoon, Chosen Company, who came to aid 2nd platoon during the battle for Vehicle Patrol Base Kahler, Afghanistan. Here, they are at Forward Operating Base Blessing, Nangalam, Afghanistan."
}]