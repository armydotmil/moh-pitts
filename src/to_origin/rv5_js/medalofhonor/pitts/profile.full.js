//Last Updated 2014-07-21 1808 ASB

$(document).ready(function(){
	profile.init();
	//masthead.init();
	
	//0000 of the day of the ceremony (EST)
	var day_of_wh = 1405915200000;
	
	//One hour before WH scheduled start time
	var wh_start_time = 1405965600000;
	//one hour after WH scheduled end time
	var wh_end_time = 1405978200000;
	
	//One hour before HOH scheduled star time
	var hoh_start_time = 1406034000000;
	//One hour after HOH scheduled end time
	var hoh_end_time = 1406048400000;
	
	var now = new Date();
	now = now.getTime();
		
	var override = GetURLParameter("time");
	override = parseInt(override);
		
	if(!isNaN(override)){
		now = override;
	}
		
	if(now < day_of_wh) {
		masthead.init();	
	}else if(now > day_of_wh && now < wh_start_time) {
		masthead.init();
		stream_ad.init(
			"Live Webcast: Medal of Honor Ceremony",
			"Watch the live webcast of Staff Sgt. Ryan Pitts' Medal of Honor ceremony today, at 3 p.m. (EDT)",
			"President Barack Obama will award former Staff Sgt. Ryan Pitts the Medal of Honor today, at the White House. Pitts will be the ninth living recipient to be awarded the Medal of Honor for actions in Iraq or Afghanistan.",
			"/e2/rv5_images/medalofhonor/pitts/profile/stream_ads/white_house.jpg",
			"The White House"
		);
	} else if(now > wh_start_time && now < wh_end_time){
		live_stream.init(
			"Live Webcast: Medal of Honor Ceremony",
			4694
		);
	} else if(now > wh_end_time && now < hoh_start_time) {
		masthead.init();
		stream_ad.init(
			"Live Webcast: Hall of Heroes Induction",
			"Watch the live webcast of Staff Sgt. Ryan Pitts' Hall of Heroes Induction ceremony, July 22, 2014 at 10 a.m. (EDT)",
			"The U.S. Army will induct former Staff Sgt. Ryan Pitts into the Pentagon's Hall of Heroes in a July 22, 2014, ceremony. The Pentagon ceremony will add Pitts' name to the distinguished roster in the Hall of Heroes, the Defense Department's permanent display of record for all recipients of the Medal of Honor.",
			"/e2/rv5_images/medalofhonor/pitts/profile/stream_ads/hall_of_heroes.jpg",
			"The Pentagon"
		);
	} else if(now > hoh_start_time && now < hoh_end_time) {
		live_stream.init(
			"Live Webcast: Hall of Heroes Induction",
			4696
		);
	} else if(now > hoh_end_time) {
		masthead.init();
	}


	
	$("#press_kit").click(function(){
		if (typeof ga === "function") {
			ga('send', 'event', 'downloads', 'zip', $(this).attr("href"));
		}
	});
	
	$("#contact").click(function(){
		if (typeof ga === "function") {
			ga('send', 'event', 'button', 'click', 'contact');
		}
	});
	
	$("#poster").click(function(){
		if (typeof ga === "function") {
			ga('send', 'event', 'downloads', 'jpg', $(this).attr("href"));
		}
	});
	
	$(window).resize(function() {
		if (parseInt($(window).width()) >= 768 && start_width < 768) {
			change_image = true;
			start_width = parseInt($(window).width());
		} else if ((parseInt($(window).width()) < 768 && start_width >= 768) || (parseInt($(window).width()) >= 992 && start_width < 992)) {
			change_image = true;
			start_width = parseInt($(window).width());
		} else if (parseInt($(window).width()) < 992 && start_width >= 992) {
			change_image = true;
			start_width = parseInt($(window).width());
		} else {
			change_image = false;
		}
		if (change_image) {
			profile.create_bullet_rows();
		}
	});
});

var stream_ad = {
	init : function(maintitle, subtitle, message, image, alt) {
		$("#content").prepend(
			$("<div>").prop({
				"id" : "stream_ad"
			}).append(
				$("<div>").prop({
					"class" : "container"
				}).append(
					$("<div>").prop({
						"class" : "section_title"
					}).append(
						$("<h2>").html(maintitle)
					)
				).append(
					$("<div>").prop({
						"id" : "stream_info"
					}).append(
						$("<div>").prop({
							"id" : "stream_info_left"
						}).append(
							$("<blockquote>").text(subtitle),
							$("<p>").text(message)
						)
					).append(
						$("<div>").prop({
							"id" : "stream_info_right"
						}).append(
							$("<img>").prop({
								"src" : image,
								"alt" : alt,
								"width" : "300",
								"height" : "169"
							})
						)
					)
				)
			)
		);
	}		
};

var live_stream = {
	
	init: function(maintitle, stream_id){
					
		var player_w = "";
		var player_h = "";
	
		$("#content").prepend(
			$("<div>").prop({
				"id" : "stream_ad"
			}).append(
				$("<div>").prop({
					"class" : "container"
				}).append(
					$("<div>").prop({
						"class" : "section_title"
					}).append(
						$("<h2>").text(maintitle)
					)
				).append(
					$("<div>").prop({
						"id" : "stream_info"
					})
				)
			)
		);
		
		$("#stream_info").html("").append(
			$("<div>").prop({
				"id" : "dvids_ad"
			}).append(
				$("<div>").prop({
					"id" : "dvids_inner"
				})
			)
		);

		$("#stream_info").prepend(
			$("<div>").prop({
				"id" : "live_stream_feed"
			})
		);
		
		var feed_width = parseInt($("#live_stream_feed").width());
		
		player_w = feed_width;
		if(player_w > 720) {
			player_w = 720;
		}
		player_h = Math.floor(feed_width * 9 / 16);
				
		$("#live_stream_feed").append(
			'<iframe width="'+player_w+'" height="'+player_h+'" scrolling="no" frameborder="0" style="border: none; overflow: hidden; width: '+player_w+'px; height: '+player_h+'px;" allowtransparency="true" src="http://www.dvidshub.net/webcast/embed/'+stream_id+'?show_description=0&width='+player_w+'&height='+player_h+'"></iframe>'
		);
		
		$("#dvids_inner").append(
			$("<p>").html("&nbsp;"),
			$("<p>").html("VIDEO PROVIDED BY <a href='http://www.dvidshub.net/'><img src='/e2/rv5_images/medalofhonor/valor24/dvids_logo.png' alt='dvids' /></a>")
		);
	}
	
}

var profile = {
	init : function() {
		var moh_document = new Documents();
		moh_document.onDocumentsObjectReady = function(context){
			moh_document = moh_document.documents;
			profile.get_bullets();			
			profile.get_biography(moh_document);
			//profile.get_units(moh_document);
		
		};
	},
	
	get_bullets : function() {
		var bullets = new Bullets();
		for(var i = 0; i < bullets.bullet_data.length; i++){
			profile.add_bullet(bullets.bullet_data[i]);
		}
		
	},
	
	get_biography : function(docs) {
		var biographies = docs[1];
		for (var i = 0; i < biographies.sections[0].text.length; i++) {
			profile.add_biography(biographies.sections[0].text[i]);
		}
	},
	
	get_units : function(docs) {
		profile.add_unit_nav(docs[2],true, false);
		profile.add_unit_view(docs[2]);
		profile.add_unit_nav(docs[3],false, true);
		profile.add_unit_view(docs[3]);
		//var links = new unit_links();
		profile.activate_unit_at(0);
	},
	
	add_unit_nav : function(unit, is_first, is_last) {
		var li = $("<li>");
		
		var a = $("<a>").prop({"href":"#"}).html(unit['title']);
		if(is_first){ a.addClass("active"); }
		
		if(is_last){ li.addClass("last"); }
		
		a.click(function(e){
			var links = $(".unit_nav ul a");
			e.preventDefault();
			links.removeClass('active');
			var index = links.index(e.target);
			links.eq(index).addClass('active');
			profile.activate_unit_at(index);
		});
				
		$(".unit_nav ul").append(
			li.append(a)
		);	
	},
	
	add_unit_view : function(unit) {
		$("#unit_history .container").append(
			$("<div>").prop({
				"class":"unit_info_box inactive"
			}).append(
				$("<div>").prop({
					"class":"unit_info_box_inner"
				}).append(
					$("<h3>").html(unit['title']),
					$("<div>").prop({
						"class":"pull_quote"
					}).append(
						$("<blockquote>").html(unit['pull_quote'])
					),
					$("<p>").html(unit['sections'][0]['text'][0]),
					$("<div>").prop({
						"class":"button"
					}).append(
						$("<a>").prop({
							"href":"#",
							"id" : unit['id']
						}).html("Read More").click(function(){
								lightbox.init("document_"+unit['id']);
								return false;
							}
						)
					)
				)
			)
		);
	},
	
	activate_unit_at : function(unit_index) {
		var unit_blocks = $(".unit_info_box");
		$(unit_blocks).addClass("inactive");
		$(unit_blocks[unit_index]).removeClass("inactive");
	},
	
	add_biography : function (bio_text){
		var html = '<p>' + bio_text + '</p>';
		$(".bio_text").append(html);
	},
	
	add_bullet : function(bullet){
		html = "<div class=\"list\"><dl>";
		html += "<dt>" + bullet.title + "</dt>";
		html += "<dd>" + bullet.content + "</dd>";
		html += "</dl></div>";
		$(".bio_bullets").append(html);
	},
	create_bullet_rows : function() { //create individual rows for each configuration
		var num_bullets = $('div.bio_bullets div.list').length;
		$("div.new").each(function () {
		    $(this).replaceWith($(this.childNodes));
		});

	}
}


function Bullets() {
	var context = this;
	this.bullet_data = [
			{
				"title" : "hometown",
				"content" : "Nashua, New Hampshire"
			},
			{
				"title" : "enlistment date",
				"content" : "January, 2003"
			},
			{
				"title" : "military occupation (mos)",
				"content" : "Forward Observer (13F)"
			},
			{
				"title" : "born",
				"content" : "1985"
			},
			{
				"title" : "unit",
				"content" : "Chosen Company, 2nd Battalion (Airborne), 503rd Infantry Regiment, 173rd Airborne Brigade"
			},
			{
				"title" : "deployments",
				"content" : "Operation Enduring Freedom VI & VIII, Afghanistan"
			}
		];
}


var viewportWidth = $(window).width();
var start_width = viewportWidth;
var change_image = true;


function GetURLParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}