//Last Updated 2014-07-16 1650 ASB
var Article = Backbone.Model.extend({
	urlRoot: '//www.army.mil/medalofhonor/pitts/get_news.php'
});

var Articles = Backbone.Collection.extend({
	model: Article,
	url: '//www.army.mil/medalofhonor/pitts/get_news.php'
});

var articles = new Articles();

var ArticleGallery = Backbone.View.extend({
	initialize: function(options) {
		for (var i = 0; i < options.rows; i++) {
			this.listenTo(articles, 'sync', this.load_articles);
		}
		articles.url += "?offset=" + options.offset + "&count=" + options.count ;
		articles.fetch();
	},
	articles_loaded: 0,
	load_articles: function() {
		var news_stories = $('<div>').prop({
			'class': 'news_stories'
		});
	
		var num_articles = 2;
					
		for (var i = 0; i < num_articles; i++) {
			if (articles.length > 0) {
				news_stories.append(build_news_cell(articles.shift()));
			} else {

				$("#news .button").hide();
			}
		}
		$("#news_section").append(news_stories);
		$(".news_story").last().addClass("last_news_story");
	}
});

function build_news_cell(article) {

	var page = ($('body').prop('id') == 'body_home') ? 'home_' : '';
	
	var window_width = parseInt($(window).width());
	
	var news_story = $('<div>').prop({
		'class': 'news_story'
	});
	var max_index = article.get("images").length - 1;
	var random_index = Math.floor((Math.random() * max_index) + 0);
	var image = "";
	if(window_width < 769) {
		image = article.get("images")[random_index].url_size1;
	} else {
		image = article.get("images")[random_index].url_size2;
	}
	var alt = article.get("images")[random_index].alt;
	var title = article.get("title");
	var max_str_length = 50;
	if(title.length > max_str_length){
		title = title.substring(0, max_str_length);
		title += "...";
	}
	
	var url = article.get("page_url");
	var description = article.get("description").substring(0, 100) + "...";
	
	var image_url = url + '?from=moh_pitts_' + page + 'news_image';
	var title_url = url + '?from=moh_pitts_' + page + 'news_text';
	var full_article_url = url + '?from=moh_pitts_' + page + 'news_full_article';
	
	var news_story_image = $('<a>').prop({'href': image_url, "target": "_blank"}).append($('<img>').prop({
		'src': image,
		'alt': alt
	}));
	var news_story_title = $('<p>').prop({
		'class': ''
	}).append($('<a>').prop({
		'class': 'news_story_title',
		'href': title_url
	}).html(title));

	return news_story.append(news_story_image).append(news_story_title);
}