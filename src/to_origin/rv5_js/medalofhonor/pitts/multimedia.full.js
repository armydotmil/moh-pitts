/* Last update 06/04/2015 by BMN */
/*global $,Backbone,_,lightbox,ArticleGallery,ExternalArticleGallery*/
var api_base_url = '//www.army.mil/api/package/';
var api_limit = '&limit=100';
var //package_id = 59122;//DEVELOPMENT
package_id = 128076;

var VideoModel = Backbone.Model.extend({
	//urlRoot: '../json/getpackage.json'//api_base_url + "getpackage?package_id=" + package_id + api_limit
	urlRoot: api_base_url + "getpackage?package_id=" + package_id + api_limit
});

var VideoCollection = Backbone.Collection.extend({
	model: VideoModel,
	//url: '../json/getpackage.json'//api_base_url + "getpackage?package_id=" + package_id + api_limit
	url: api_base_url + "getpackage?package_id=" + package_id + api_limit
});

var MediaModel = Backbone.Model.extend({
	//urlRoot: '../json/getimages.json' //
	url: api_base_url + "getpackageimages?package_id=" + package_id + api_limit
});

var MediaCollection = Backbone.Collection.extend({
	model: MediaModel,
	//url: '../json/getimages.json'//
	url: api_base_url + "getpackageimages?package_id=" + package_id + api_limit
});

var MediaView = Backbone.View.extend({
	videoCollection: "",
	collection: "",
	media_ids: [],
	bc_ids: [],
	initialize: function() {
		this.collection = new MediaCollection();
		this.videoCollection = new VideoCollection();
		this.videoMap = {};
		this.listenTo(this.videoCollection, 'sync', this.updateIds);
		this.listenTo(this.collection, 'sync', this.loadLargeImages);
		this.listenTo(this.collection, 'sync', this.loadImages);
		resizeTablet = _.bind(this.resizeTablet, this);
		resizeDesktop = _.bind(this.resizeDesktop, this);
		resizeMobile = _.bind(this.resizeMobile, this);
		resizeImages = _.bind(this.resizeImages, this);
		resizeAll = _.bind(this.resizeAll, this);
		this.videoCollection.fetch();
	},
	video_map: function() {
		var body = this.videoCollection.models[0].get('body').split("\n"),
		map = {}, position;
		$(".media a.video").each(function(index, item) {
			position = $(".media a").index($(this));
			map[position] = {
				"media_id": body[index].split(":")[0],
				"bc_id": body[index].split(":")[1]
			};
		});
		return map;
	},
	updateIds: function() {
		var body = this.videoCollection.models[0].get('body').split("\n");

		for (var i = 0; i < body.length; i++) {
			this.media_ids.push(body[i].split(":")[0]);
			this.bc_ids.push(body[i].split(":")[1]);
		}

		this.collection.fetch();
	},
	loadImages: function() {
		var window_size = parseInt($(window).width()),
		start = $(".media a").length,
		end = this.collection.length;

		for (var i = start; i <= end; i++) {
			this.loadImage(window_size, i);
		}

		resizeAll();
	},
	loadLargeImages: function() {
		var window_size = parseInt($(window).width());
		for (var i = 0; i < 3; i++) {
			this.loadLargeImage(window_size, i);
		}

		resizeAll();
	},
	loadLargeImage: function(size, index) {
		var mobile = (size <= 480),
		tablet = (size > 480 && size <= 768),
		desktop = (size > 768),
		png = "/768.jpg", css_class = "big_img";
		switch (true) {
			case (desktop && index < 3):
				png = "/311.jpg";
				css_class = "big_img";
				break;
			case (tablet && index < 2):
				png = "/359.jpg";
				css_class = "big_img";
				break;
			case (mobile):
				png = "/451.jpg";
				css_class = "big_img";
				break;
			default:
				break;
		}

		this.buildImage(png, css_class, index);
	},
	loadImage: function(size, index) {
		var png = "/768.jpg",
		css_class = "big_img",
		mobile = (size <= 480),
		tablet = (size > 480 && size <= 768),
		desktop = (size > 768);
		switch (true) {
			case (desktop && index >= 3):
				png = "/228.jpg";
				css_class = "small_img";
				break;
			case (tablet && index >= 2):
				png = "/172.jpg";
				css_class = "small_img";
				break;
			case (mobile):
				png = "/451.jpg";
				css_class = "big_img";
				break;
			default:
				break;
		}

		this.buildImage(png, css_class, index);

	},

	
	buildImage: function(png, css_class, index) {
		if (this.collection.models[index]) {
			var container = $(".media"),
			model = this.collection.models[index],

			title = model.get("title"),
			id = model.get("id"),
			image = "/e2/rv5_images/medalofhonor/pitts/multimedia/" + id + png,
			alt = model.get("caption"),

			image_wrap, play_button, caption_box, caption_p, img,

			link = $("<a>").prop({
				'href': 'javascript:;',
				'class': css_class,
				'id': 'image_' + index
			}).click(function() {
				lightbox.init($(this).attr("id"));
			});

			if ($.inArray(id.toString(), this.media_ids) > -1) {
				link.addClass('video');
			} else {
				link.addClass('image');
			}

			image_wrap = $("<div>").prop({
				'class' : 'image_wrap'
			});			
			
			play_button = $("<div>").prop({'class' : 'play_button_overlay'});			
			
			caption_box = $("<div>").prop({'class' : 'caption_overlay'});
			
			caption_p = $("<p>").text(title);

			img = $("<img>").prop({
				'alt': alt,
				'src': image
			}).on("error", function() {
				$(this).parent().hide();
				resizeAll();
			}).on("load",function(){
				$(this).parent().show();
				//resizeAll();
			});

			container.append(link.append(image_wrap.append(img).append(play_button).append(caption_box).append(caption_p)));
		}
	},


	resizeImages: function() {
		$(window).resize(function() {
			resizeAll();
		});
	},
	resizeDesktop: function() {
		$(".media a:visible:lt(3)").addClass("big_img").removeClass("small_img");
		$(".media a:visible:gt(2)").addClass("small_img").removeClass("big_img");
		$(".big_img img").each(function() {
			var src = $(this).attr('src');
			src = src.replace(/(.*)\/.*(\.jpg$)/i, '$1/311$2');
			$(this).attr('src', src);
		});

		$(".small_img img").each(function() {
			var src = $(this).attr('src');
			src = src.replace(/(.*)\/.*(\.jpg$)/i, '$1/228$2');
			$(this).attr('src', src);
		});
	},
	resizeMobile: function() {
		$(".media a").addClass("big_img").removeClass("small_img");
		$(".big_img img").each(function() {
			var src = $(this).attr('src');
			src = src.replace(/(.*)\/.*(\.jpg$)/i, '$1/451$2');
			$(this).attr('src', src);
		});
	},
	resizeTablet: function() {
		$(".media a:visible:lt(2)").addClass("big_img").removeClass("small_img");
		$(".media a:visible:gt(1)").addClass("small_img").removeClass("big_img");
		$(".big_img img").each(function() {
			var src = $(this).attr('src');
			src = src.replace(/(.*)\/.*(\.jpg$)/i, '$1/359$2');
			$(this).attr('src', src);
		});
		$(".small_img img").each(function() {
			var src = $(this).attr('src');
			src = src.replace(/(.*)\/.*(\.jpg$)/i, '$1/172$2');
			$(this).attr('src', src);
		});		
	},
	resizeAll: function() {
		var width = parseInt($(window).width());
		var mobile = (width <= 480);
		var tablet = (width > 480 && width <= 768);
		var desktop = (width > 768);
		switch (true) {
			case (desktop):
				resizeDesktop();
				break;
			case (tablet):
				resizeTablet();
				break;
			case (mobile):
				resizeMobile();
				break;
			default:
				break;
		}
	}
});

var app = new MediaView();

$(document).ready(function() {
	var tag = document.createElement('script');
	tag.src = "https://www.youtube.com/player_api";
	var firstScriptTag = document.getElementsByTagName('script')[0];
	firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
	
	masthead.init();
	var articleGallery = new ArticleGallery({
		rows: 3,
		offset: 1,
		count: 100
	});
	var extArticleGallery = new ExternalArticleGallery();

	$(".media").waypoint(function() {
		app.loadImages();
	}, {
		offset: 'bottom-in-view'
	});

	app.resizeImages();

	$(".multimedia_nav ul li a").click(function() {
		var type = $(this).prop("href");
		type = type.split("#")[1];

		$(".multimedia_nav ul li a").removeClass();

		$(this).addClass("active");
		if (type == "videos") {
			$(".media .image").hide();
			$(".media .video").show();
		} else if (type == "images") {
			$(".media .video").hide();
			$(".media .image").show();
		} else {
			$(".media a").show();
		}

		app.resizeAll();

	});
});
