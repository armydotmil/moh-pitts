//Last Updated 2014-08-05 0946 BMN

var footer_data =
[

	{
		"title": "Medal of Honor",
		"links": [
			{
				"link": "https://www.army.mil/medalofhonor/",
				"text": "Army.mil: Medal of Honor"
			},
			{
				"link": "https://www.army.mil/standto/archive_2014-07-21",
				"text": "STAND-TO! Medal of Honor for Staff Sgt. Ryan Pitts"
			},
			{
				"link": "http://valor.defense.gov/",
				"text": "Military Awards for Valor"
			},
			{
				"link": "http://www.history.army.mil/moh/index.html",
				"text": "Center of Military History"
			},
			{
				"link": "http://www.tioh.hqda.pentagon.mil/",
				"text": "U.S. Army Institute of Heraldry"
			},
			{
				"link": "http://www.tioh.hqda.pentagon.mil/Catalog/Heraldry.aspx?HeraldryId=15743&CategoryId=9360&grp=2&menu=Uniformed%20Services&ps=24&p=0&hilite=medal%20of%20honor%20flag",
				"text": "Medal of Honor Flag"
			},
			{
				"link": "http://www.cmohs.org/",
				"text": "Official Site of the Congressional Medal of Honor Society"
			}
		]
	},
	{
		"title": "Support",
		"links": [
			{
				"link": "https://www.army.mil/info/armylife/veterans/",
				"text": "Army Veterans Resources"
			},
			{
				"link": "http://www.va.gov",
				"text": "U.S. Department of Veterans Affairs"
			},
			{
				"link": "https://www.army.mil/veterans/",
				"text": "U.S. Army Veterans: News"
			},
			{
				"link": "http://myarmybenefits.us.army.mil/",
				"text": "My Army Benefits"
			},
			{
				"link": "https://www.army.mil/families",
				"text": "Army Families"
			},
			{
				"link": "https://www.army.mil/readyandresilient/",
				"text": "Ready and Resilient"
			},
			{
				"link": "https://www.army.mil/soldierforlife/",
				"text": "Soldier for Life"
			}
		]
	},
	{
		"title": "Army.mil Resources",
		"links": [
			{
				"link": "https://www.army.mil/contact/",
				"text": "Contact Us"
			},
			{
				"link": "https://www.rmda.army.mil/foia/RMDA-FOIA-Division.html",
				"text": "FOIA"
			},
			{
				"link": "http://arba.army.pentagon.mil/documents/No%20Fear%20FY10%20Q1.pdf",
				"text": "No Fear Act"
			},
			{
				"link": "https://www.army.mil/accessibility/",
				"text": "Accessibility/Section 508"
			},
			{
				"link": "https://www.inscom.army.mil/isalute/",
				"text": "iSALUTE"
			},
			{
				"link": "https://www.army.mil/standto/",
				"text": "STAND-TO!"
			},
			{
				"link": "https://www.army.mil/values/",
				"text": "Army Values"
			},
			{
				"link": "https://www.army.mil/valor/",
				"text": "U.S. Army Stories of Valor"
			}
		]
	}
];

var menu_data =
[
	{
		"title" : "Profile",
		"link" : "profile/index.html",
		"inner_links" : [
			{
				"text" : "Biography",
				"link" : "#biography"
			},
			{
				"text" : "Official Citation",
				"link" : "#citation_image"
			},
			{
				"text" : "Unit Information",
				"link" : "#unit_history"
			}
		]
	},
	{
		"title" : "The Battle",
		"link" : "battle/index.html",
		"inner_links" : [
			{
				"text" : "Battlescape",
				"link" : "#title"
			},
			{
				"text" : "The Team",
				"link" : "#chosen_few"
			}
		]
	},
	{
		"title" : "Media",
		"link" : "media/index.html",
		"inner_links" : [
			{
				"text" : "News",
				"link" : "#news_container"
			},
			{
				"text" : "Multimedia",
				"link" : "#multimedia_section"
			}
		]
	}
];

var menu = {

	get_current_url : function() {
		if (!window.location.origin) {
			window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
		}
		return window.location.origin + window.location.pathname;
	},

	current_url : '',
	is_open : false,
	path : "https://www.army.mil/medalofhonor/pitts/",

	init : function() {
		var items = menu_data;
		if (!menu.current_url) {
			menu.current_url = menu.get_current_url();
		}
		$('body').prepend(
			$("<div>").prop({
				"id" : "top_bar"
			}).append(
				$("<div>").prop({
					"class" : "top_bar_container"
				}).append(
					$("<div>").prop({
						"id" : "logo"
					})
				).append(
					$("<div>").prop({
						"id" : "menu_button"
					}).append(
						$("<div class='line'>"),
						$("<div class='line'>"),
						$("<div class='line last'>")
					),
					$("<div>").prop({
						"id" : "menu"
					}).append(
						$("<div>").prop({
							"id" : "menu_inner"
						})
					)
				)
			)
		);
		
		for (var i = 0; i < items.length; i++) {
			var last_class = (i === items.length - 1) ? "last" : "";
			$('#menu #menu_inner').append(
				menu.make_item(items[i], last_class)
			);
		}

		menu.bind_events();
	},

	make_item : function(item, last_class) {
		var active_class = (menu.match_path(item.link)) ? "active" : "";
		var insertion = 
			$("<div>").prop({
				"class" : "menu_section"
			}).append(
				menu.make_header(item, active_class)
			);
		if (item.inner_links) {
			insertion.append(
				menu.make_link(item, active_class, last_class)
			);
		}
		
		return insertion;
	},

	make_header : function(item, active_class) {
		var header = $("<h4>").append(
			$("<a>").prop({
				"href": menu.path + item.link
			}).text(item.title).append(
				$("<span>").prop({
					"class" : active_class
				})
			)
		);
		return header;
	},
	
	make_link : function(item, active_class, last_class) {
		var list = $("<ul>").prop({
			"class" : active_class + " " + last_class
		});
		var main_link = item.link;
		for(var i = 0; i < item.inner_links.length; i++) {
			list.append(
				$("<li>").append(
					$("<a>").prop({
						"href": menu.path + main_link + item.inner_links[i].link
					}).text(item.inner_links[i].text)
				)
			);
		}
		return list;
	},

	match_path : function(item_url) {
		var full_item_url = menu.path + item_url;
		if (menu.current_url == full_item_url || (menu.current_url + "index.html") == full_item_url) {
			return true;
		}
		return false;
	},

	show_menu : function() {
		var window_width = parseInt($(window).width());
		var menu_width;
		
		if(menu.is_open)
		{
			menu_width = 66;
			menu.is_open = false;
			if (typeof ga === "function") {
				ga('send', 'event', 'menu', 'close', 'top_bar_button');
			}
		} else {
			menu_width = (window_width > 480) ? (window_width / 2) + 66 : window_width;
			menu.is_open = true;
			if (typeof ga === "function") {
				ga('send', 'event', 'menu', 'open', 'top_bar_button');
			}
		}
		$("#menu_button").animate({
			"right" : menu_width - 66
		}, {duration: 350, queue: false});
		$("#menu").animate({
			width: menu_width
		}, {duration: 350, queue: false});
	},

	bind_events : function() {
		//If menu is open and user clicks outside of the menu,
		//close the menu
		
		var click_handle = (Modernizr.touch) ? "click touchstart" : "click";
		
		$('html').on(click_handle, function() {
			if(menu.is_open)
			{
				menu.show_menu();
				if (typeof ga === "function") {
					ga('send', 'event', 'menu', 'close', 'outside_click');
				}
			}
		});

		//Unless click is inside menu
		$('#menu').on(click_handle, function(e) {
			e.stopPropagation();
		});

		$('.menu_section ul li a').on(click_handle, function(e) {
			var window_width = $(window).width();
			var off_set = (window_width <= 979) ? 66 : 50;
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
				 || location.pathname.replace(/^\//,'') + "index.html" == this.pathname.replace(/^\//,'')
				 && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top - off_set // to compensate for the header bar
					}, 1000);
					if ($(window).width() <= 979) {
						menu.show_menu();
					}
					return false;
				}
			} else {
				return true;
			}
		});
		
		$(".menu_section h4").on(click_handle, function(e) {
			if ($(window).width() <= 979) {
				e.preventDefault();
				var active = $(this).next('ul');
				var carat = $(this).find('span');
				var carat_list = $(".menu_section h4 span").not(carat);
				var lists = $(".menu_section ul").not(active);
				var section = $(this).text();
				section = section.replace(/\s/g, "_").toLowerCase();
				lists.slideUp(function() { // slide up all others and remove the active class from them too
					$(this).css('display','').removeClass('active');
				});
				carat_list.removeClass('active');
				if (active.hasClass('active')) { // either add the active class to the one clicked or remove it
					active.slideUp(function() {
						$(this).css('display','').removeClass('active');
					});
					carat.removeClass('active');
				} else {
					active.slideDown(function() {
						$(this).css('display','').addClass('active');
					});
					carat.addClass('active');
					if (typeof ga === "function") {
						ga('send', 'event', 'menu', 'click', 'open_section_' + section);
					}
				}
			}
		})
		.on('mouseover', function() {
			if ($(window).width() > 979) {
				var section = $(this).find('a').text();
				section = section.replace(/\s/g, "_").toLowerCase();
				if (typeof ga === "function") {
					ga('send', 'event', 'menu', 'mouseover', section);
				}
			}
		});

		$("#menu_button").on(click_handle, function() {
			menu.show_menu();
			return false;
		});
		
		$(window).resize(function() {
			if ($(this).width() > 979) {
				$("#menu").css({
					"height": "",
					"width" : ""
				});
				menu.is_open = false;
			}
		});
	}
};

var footer = {

	init : function() {
		var items = footer_data;
		$('body').append(
			$('<div>').addClass('footerWrap').append(
				$('<div>').addClass('footer').append(
					$('<div>').addClass('container')
				)
			)
		);
		$.each(items, function(i, item) {
			var footer_class = (i < 2) ? 'wide' : 'narrow';
			$('.footer .container').append(
				$('<div>').addClass('footer_block ' + footer_class).html(footer.make_item(item))
			);
		});
		
		var footer_block = $('<div>').addClass('footer_block logo');
		
		var link = $('<a>').prop({
			"href" : "https://www.army.mil/"
		});	
		
		var image = $('<img>').prop({
			"src" : "/e2/rv5_images/features/army_star_neg.png",
			"alt" : "US Army Logo",
			"width" : "65",
			"height" : "81"
		});	
		
		link.append(image);
		footer_block.append(link);
		$(".footer .container").append(footer_block);
		
		footer.bind_events();
	},

	make_item : function(item) {
		var insertion = "";

		insertion += footer.make_header(item);
		insertion += footer.make_link(item);

		return insertion;
	},

	make_header : function(item) {
		var insertion = "<p class='footerheading'>" + item.title + "<span class='expand'>+</span></p>";
		return insertion;
	},

	make_link : function(item) {
		var insertion = "<ul>";
		$.each(item.links, function(i, linkinfo) {
			if (!linkinfo.link) {
				insertion += "<li>" + linkinfo.text + "</li>";
			} else {
				insertion += "<li><a href='" + linkinfo.link + "'>" + linkinfo.text + "</a></li>";
			}
		});
		insertion += "</ul>";
		return insertion;
	},

	bind_events : function() {
		$('.footerheading').on('click', function() {
			if ($(this).find("span").is(":visible")) {
				var lists = $(this).parent().siblings().find("ul");
				var next_list = $(this).next('ul');
				if (next_list.hasClass("active")) {
					next_list.removeClass("active");
				} else {
					lists.removeClass("active");
					next_list.addClass("active");
				}
			}
		});
	}
};

$(window).scroll(function() {
	var collapse_point = 30,
	scroll_top = $(window).scrollTop();

	if (scroll_top > collapse_point) {
		$("#top_bar,#menu").addClass("collapsed");
		$("#featureBar").addClass("fb_collapsed");
	} else {
		$("#top_bar,#menu").removeClass("collapsed");
		$("#featureBar").removeClass("fb_collapsed");
	}
});

$(document).ready(function() {
	menu.init();
	footer.init();

	//Detect high res displays (Retina, HiDPI, etc...)
	Modernizr.addTest('highresdisplay', function(){
		if (window.matchMedia) {
			var mq = window.matchMedia("only screen and (-moz-min-device-pixel-ratio: 1.3), only screen and (-o-min-device-pixel-ratio: 2.6/2), only screen and (-webkit-min-device-pixel-ratio: 1.3), only screen and (min-device-pixel-ratio: 1.3), only screen and (min-resolution: 1.3dppx)");
			if(mq && mq.matches) {
				return true;
			}
	    }
	});
	
	 if(typeof addthis != "undefined"){
		addthis.layers({
			'theme' : 'transparent',
			'share' : {
				'position' : 'left',
				'services' : 'facebook,twitter,google_plusone_share,pinterest',
				'offset': {
					'top':'230px'
				}
			},
			'thankyou' : 'false',
			'responsive' : {
				'maxWidth' : '768px'
			},
			 'mobile' : {
				'buttonBarPosition' : 'bottom',
				'buttonBarTheme' : 'light',
				'mobile' : true
			}
		});
	}
	

	//send an event to GA when a link is clicked
	$("a").on('click', function(event) {
		if ($(this).attr('href').match(/pitts/g)
		|| $(this).attr('href').match(/#/g)
		&& $(this).attr('href') !== "#") {
			var dest_uri = $(this).attr('href').split('/');
			var current_uri = window.location.pathname.split('/');
			var i = dest_uri.length - 1;
			var j = current_uri.length - 1;

			var dest_pagename = dest_uri[i - 1];
			var current_pagename = current_uri[j - 1];
			var section_name = current_pagename;
			var send_event = true;

			if ($.inArray('#', dest_uri[i]) > -1) {
				dest_pagename = dest_uri[i].split('#')[1];
			}

			if (current_pagename.match(/.html/g)) {
				current_pagename = current_uri[j - 2];
			}

			if ($(this).parents().hasClass('menu_section')) {
				section_name = 'menu';
				if($(this).find('span').length && menu.is_open) { // clicking on menu heading when mobile menu is open
					send_event = false;
				}
			}

			var description = current_pagename + "_" + dest_pagename;

			if (send_event && typeof ga === "function") {
				ga('send', 'event', section_name, 'click', description);
			}

		}
	});

});