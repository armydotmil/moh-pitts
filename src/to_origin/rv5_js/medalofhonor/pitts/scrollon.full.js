//Last Updated 2014-07-16 1650 ASB
(function($) {
		
	    var pluginName = 'scrollon',
	           defaults = {
	               offset: 100,
				   callback: function(e){
					   //console.log("BOTTOM REACHED");
				   }
	           };
			   
		function Plugin( element, options ) {
			           this.element = element;
			  
			           this.options = $.extend( {}, defaults, options) ;
        
			           this._defaults = defaults;
			           this._name = pluginName;
        
			           this.init();
		}
		
		Plugin.prototype.init = function () {
			var bottom_reached = false;
			var offset = this.options.offset ? this.options.offset: this._defaults.offset;
			var callback = typeof this.options.callback === "function" ? this.options.callback :
			this._defaults.callback;
			var element =  $(this.element);
			$(window).scroll(function() {
				var bottom = element.position().top + element.innerHeight() * (offset/100);
				if ($(window).scrollTop() > bottom && !bottom_reached) {
					bottom_reached = true;
					if(typeof callback === "function"){
						callback(element);
					}
				} else {
					bottom_reached = false;
				}
			});
		};
			  
	
		$.fn[pluginName] = function ( options ) {
		        return this.each(function () {
		             if (!$.data(this, 'plugin_' + pluginName)) {
 		                $.data(this, 'plugin_' + pluginName,  new Plugin( this, options ));
 		             }
		        });
		  }
})(jQuery);
