//Last Updated 2014-08-14 1043 IKC
//need to adjust the height for the top padding that is in place
//because of the nav bar

var masthead = {

	base_url : "/e2/rv5_images/medalofhonor/",
	//base_url: "http://localhost:8888/ardev/frontend/trunk/httpdocs/_development/",
	directory : "",
	orig_w : 3648,
	orig_h : 2052,

	init : function() {
		var viewportWidth = masthead.getViewportWidth();
		var start_width = viewportWidth;
		var need_new_image = true;
		
		var full_images = $(".full_width_image");
		var full_image_ids = new Array();
		
		for(var i = 0; i < full_images.length; i++)
		{
			masthead.getMastheadImage(viewportWidth, need_new_image, full_images[i]);
		}
		
		$(window).resize(function() {
			
			viewportWidth = parseInt(masthead.getViewportWidth());
			
			if(start_width > viewportWidth){
				need_new_image = false;
			} else {
				need_new_image = true;
				start_width  = viewportWidth;
			}
			for(var i = 0; i < full_images.length; i++)
			{
				masthead.getMastheadImage(viewportWidth, need_new_image, full_images[i]);
			}
		});
	},

	getViewportWidth : function() {
		if (document.body && document.body.offsetWidth) {
			winW = document.body.offsetWidth;
		}
		if (document.compatMode=='CSS1Compat' && document.documentElement && document.documentElement.offsetWidth ) {
			winW = document.documentElement.offsetWidth;
		}
		if (window.innerWidth && window.innerHeight) {
			winW = window.innerWidth;
		}

		return winW;
	},
	
	setMastheadImage : function(img_width, need_new_image, full_image_elm) {
		
		var opts = {
			lines: 13, // The number of lines to draw
			length: 20, // The length of each line
			width: 10, // The line thickness
			radius: 30, // The radius of the inner circle
			corners: 1, // Corner roundness (0..1)
			rotate: 0, // The rotation offset
			direction: 1, // 1: clockwise, -1: counterclockwise
			color: '#000', // #rgb or #rrggbb or array of colors
			speed: 1, // Rounds per second
			trail: 60, // Afterglow percentage
			shadow: false, // Whether to render a shadow
			hwaccel: false, // Whether to use hardware acceleration
			className: 'spinner', // The CSS class to assign to the spinner
			zIndex: 2, // The z-index (defaults to 2000000000)
			top: '50%', // Top position relative to parent
			left: '50%' // Left position relative to parent
		};
		
		var spinner = new Spinner(opts);
		
		img_w = img_width;
		img_h = img_w * masthead.orig_h / masthead.orig_w;
		
		$(full_image_elm).height(($(full_image_elm).width() * img_h / img_w));
		$(full_image_elm).width("100%");
		if(need_new_image){
			if (!$(full_image_elm).find('.spinner').length) {
				spinner.spin(full_image_elm);
			}
			//$(full_image_elm).css("background-image", "url("+masthead.base_url+"_images/pitts/"+masthead.directory+"/"+img_w+".jpg)");
			$('<img>').load(function() {
				$(full_image_elm).css("background-image", "url("+$(this).attr('src')+")");
				spinner.stop();
			}).attr('src', masthead.base_url+"pitts/"+masthead.directory+"/"+img_w+".jpg");
		}
	},
	
	getMastheadImage : function(view_width, need_new_image, full_image_elm) {
		var elm_id = $(full_image_elm).attr("id");		
		
		switch(elm_id)
		{
			case "profile_image":
				masthead.orig_w = 2500;
				masthead.orig_h = 1406;
				masthead.directory = "profile/masthead";
				
				//If the browser doesn't support svg, change directory to include image with text already on the graphic
				if(!Modernizr.svg){
					masthead.directory = "profile/masthead_ie";
				}
				
			break;
			
			case "citation_image":
				masthead.orig_w = 2500;
				masthead.orig_h = 1406;
				masthead.directory = "profile/citation";
			break;
			
			case "media_masthead":
				masthead.orig_w = 2500;
				masthead.orig_h = 1406;
				masthead.directory = "media/masthead";
			break;
			
			
			case "operation_masthead":
				masthead.orig_w = 2304;
				masthead.orig_h = 1296;
				masthead.directory = "battle/masthead";
				
			break;
			
			case "operation_map":
				masthead.orig_w = 2500;
				masthead.orig_h = 1268;
				masthead.directory = "battle/map";
			break;
			
			case "topside":
				masthead.orig_w = 2500;
				masthead.orig_h = 1074;
				masthead.directory = "battle/topside";
			break;

			case "chosen_few" : 
				masthead.orig_w = 2500;
				masthead.orig_h = 1406;
				masthead.directory = "battle/fallen";
			break;
			
			case "news" :
				masthead.orig_w = 2500;
				masthead.orig_h = 1406;
				masthead.directory = "news/masthead";
			break;	
			
			case "ceremony_image":
				masthead.orig_w = 2500;
				masthead.orig_h = 1406;
				masthead.directory = "ceremony/masthead";
			break;			
			
            default:
				masthead.orig_w = 2500;
				masthead.orig_h = 1406;
				masthead.directory = "news/" + elm_id;

		}
		var img_h = "";
		var img_w = "";
		
		var image_sizes = [320, 360, 480, 720, 768, 992, 1024, 1200, 1280, 1366, 1440, 1600, 1680, 1920, 2500];
		
		if(view_width <= image_sizes[0]){
		
			masthead.setMastheadImage(image_sizes[0], need_new_image, full_image_elm);
			
		} else if(view_width > image_sizes[0] && view_width <= image_sizes[1]){
		
			masthead.setMastheadImage(image_sizes[1], need_new_image, full_image_elm);
			
		} else if(view_width > image_sizes[1] && view_width <= image_sizes[2]){
		
			masthead.setMastheadImage(image_sizes[2], need_new_image, full_image_elm);
			
		} else if(view_width > image_sizes[2] && view_width <= image_sizes[3]){
		
			masthead.setMastheadImage(image_sizes[3], need_new_image, full_image_elm);
			
		} else if(view_width > image_sizes[3] && view_width <= image_sizes[4]){
		
			masthead.setMastheadImage(image_sizes[4], need_new_image, full_image_elm);
			
		} else if(view_width > image_sizes[4] && view_width <= image_sizes[5]){
		
			masthead.setMastheadImage(image_sizes[5], need_new_image, full_image_elm);
			
		} else if(view_width > image_sizes[5] && view_width <= image_sizes[6]){
		
			masthead.setMastheadImage(image_sizes[6], need_new_image, full_image_elm);
			
		} else if(view_width > image_sizes[6] && view_width <= image_sizes[7]){
		
			masthead.setMastheadImage(image_sizes[7], need_new_image, full_image_elm);
			
		} else if(view_width > image_sizes[7] && view_width <= image_sizes[8]){
		
			masthead.setMastheadImage(image_sizes[8], need_new_image, full_image_elm);
			
		} else if(view_width > image_sizes[8] && view_width <= image_sizes[9]){
		
			masthead.setMastheadImage(image_sizes[9], need_new_image, full_image_elm);
			
		} else if(view_width > image_sizes[9] && view_width <= image_sizes[10]){
		
			masthead.setMastheadImage(image_sizes[10], need_new_image, full_image_elm);
			
		} else if(view_width > image_sizes[10] && view_width <= image_sizes[11]){
		
			masthead.setMastheadImage(image_sizes[11], need_new_image, full_image_elm);
			
		} else if(view_width > image_sizes[11] && view_width <= image_sizes[12]){
		
			masthead.setMastheadImage(image_sizes[12], need_new_image, full_image_elm);
			
		} else if(view_width > image_sizes[12] && view_width <= image_sizes[13]){
		
			masthead.setMastheadImage(image_sizes[13], need_new_image, full_image_elm);
			
		} else if(view_width > image_sizes[13] && view_width <= image_sizes[14]){
		
			masthead.setMastheadImage(image_sizes[14], need_new_image, full_image_elm);
			
		} else if(view_width > image_sizes[14]){
		
			masthead.setMastheadImage(image_sizes[14], need_new_image, full_image_elm);
			
		}
	}
};
