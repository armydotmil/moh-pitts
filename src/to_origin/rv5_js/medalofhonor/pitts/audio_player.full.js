//Last Updated 2014-07-16 1650 ASB
var audio_player = {

	no_flash_no_html5 : false,
	has_flash : true,
	timer: null,
	audio: [],
	audio_files_path: "/e2/rv5_other/medalofhonor/pitts/audio_files/",
	now_playing: '',
	is_ended: false,

	detect_flash : function() {
		// check that user's browser supports audio elem
		if (Modernizr.audio && (Modernizr.audio.mp3 || Modernizr.audio.wav)) {
			// load html5 audio elem
			audio_player.has_flash = false;
		} else {
			var player_version = swfobject.getFlashPlayerVersion();
			if (player_version.major >= 8) {
				// load youtube player
				audio_player.has_flash = true;
			} else {
				// show that they need flash
				audio_player.no_flash_no_html5 = true;
				alert("You need to install Adobe Flash to fully experience this page. \n\nPlease visit http://get.adobe.com/flashplayer/ to install.");
				if (typeof ga === "function") {
					ga('send', 'event', 'audio', 'error', 'no_flash_no_html5');
				}
			}
		}
	},

	add_player: function(first_item) {
		if (audio_player.has_flash) {
			var params = {
				allowScriptAccess: "always",
				audio_id : first_item.youtube_id
			};
			var atts = { id: "operation_audio_player" };
			swfobject.embedSWF("http://www.youtube.com/v/" + first_item.youtube_id + "?enablejsapi=1&playerapiid=operation_audio_player&version=3",
				"operation_player", "1", "1", "8", null, null, params, atts);
		} else {
			$('#operation_player').append(
				$('<audio>').prop({
					'id' : 'operation_audio_player',
					'preload' : 'auto'
				})
			);
		}
	},

	play_flash : function(audio_id) {
		// click handle for flash obj
		var ytplayer = document.getElementById("operation_audio_player");
		var current_audio_id = $('#operation_audio_player').find('param[name="audio_id"]').val();
		var player_state = ytplayer.getPlayerState();
		var audio_player_id = $('#'+audio_id).parent('.audio_player').prop('id');
		var percent_played;

		if (audio_id != current_audio_id) { // different audio track - call loadVideoById with the new audio id
			// only send jump event the first time if the user listened to the first clip, not if they skipped it, or if they listened to the whole thing
			if (audio_player.now_playing && !audio_player.is_ended) {
				var old_player_id = $('#'+current_audio_id).parent('.audio_player').prop('id');
				percent_played = audio_player.get_progress(old_player_id);
				if (typeof ga === "function") {
					ga('send', 'event', 'audio', 'jump', old_player_id, percent_played);
				}
			}
			audio_player.clear_progress();
			$('#operation_audio_player').find('param[name="audio_id"]').val(audio_id);
			ytplayer.loadVideoById(audio_id);
			audio_player.now_playing = audio_id;
			audio_player.show_loading(ytplayer.getPlayerState(), $('#'+audio_id)); // can't use player_state var here, since there is a new state after calling loadVideoById
			if (typeof ga === "function") {
				ga('send', 'event', 'audio', 'play', audio_player_id, 0);
			}
		} else {
			if (player_state != 1) { // not playing - play the current audio
				audio_player.now_playing = audio_id;
				ytplayer.playVideo();
				audio_player.show_loading(ytplayer.getPlayerState(), $('#'+audio_id));
				percent_played = audio_player.get_progress(audio_player_id);
				if (typeof ga === "function") {
					ga('send', 'event', 'audio', 'play', audio_player_id, percent_played);
				}
			} else { // playing - clear interval
				audio_player.clear_progress();
				ytplayer.pauseVideo();
				percent_played = audio_player.get_progress(audio_player_id);
				if (typeof ga === "function") {
					ga('send', 'event', 'audio', 'pause', audio_player_id, percent_played);
				}
			}
		}
	},

	play_html5 : function(audio_id) {
		// click handle for html5 audio elem
		var file_type = '';
		var audio_player_elem = document.getElementById("operation_audio_player");
		var audio_player_id = $('#'+audio_id).parent('.audio_player').prop('id');
		var percent_played;

		if (audio_id != audio_player.now_playing) {
			// only send jump event the first time if the user listened to the first clip, not if they skipped it, or if they listened to the whole thing
			if (audio_player.now_playing && !audio_player.is_ended) {
				var old_player_id = $('#'+audio_player.now_playing).parent('.audio_player').prop('id');
				percent_played = audio_player.get_progress(old_player_id);
				if (typeof ga === "function") {
					ga('send', 'event', 'audio', 'jump', old_player_id, percent_played);
				}
			}
			if (!audio_player_elem.paused) {
				audio_player_elem.pause();
				audio_player_elem.currentTime = 0;
			}
			$('audio#operation_audio_player').empty();
			for (var i=0; i < audio_player.audio.length; i++) {
				if (audio_id == audio_player.audio[i].id) {
					for (var j=0;j < audio_player.audio[i].audio_files.length; j++) {
						file_type = audio_player.audio[i].audio_files[j].split('.')[1];
						var source_type = (file_type == 'mp3') ? 'audio/mpeg' : 'audio/' + file_type;
						$('audio#operation_audio_player').append( // append each source to audio
							$('<source>').prop({
								'src' : audio_player.audio_files_path + audio_player.audio[i].audio_files[j],
								'type' : source_type
							})
						);
					}
				}
			}
			audio_player.clear_progress();
			audio_player.now_playing = audio_id;
			audio_player.show_loading(1, $('#'+audio_id));
			audio_player_elem.load(); // load new source
			if (typeof ga === "function") {
				ga('send', 'event', 'audio', 'play', audio_player_id, 0);
			}
		} else {
			if (audio_player_elem.paused) {
				audio_player_elem.play();
				audio_player.show_loading(1, $('#'+audio_id));
				percent_played = audio_player.get_progress(audio_player_id);
				if (typeof ga === "function") {
					ga('send', 'event', 'audio', 'play', audio_player_id, percent_played);
				}
			} else {
				audio_player_elem.pause();
				audio_player.clear_progress();
				percent_played = audio_player.get_progress(audio_player_id);
				if (typeof ga === "function") {
					ga('send', 'event', 'audio', 'pause', audio_player_id, percent_played);
				}
			}
		}
	},

	show_loading : function(state, elem) {
		if (audio_player.now_playing) {
			var img_class;
			if (state == -1 || state == 3) {
				img_class = 'loading';
			} else if (state == 1) {
				img_class = 'playing';
			} else if (state == 2) {
				img_class = '';
			}
			$(elem).removeClass('loading playing').addClass(img_class);
		}
	},

	clear_progress : function() {
		$('.play_button').removeClass('playing'); // remove playing class from all
		
		if (audio_player.timer) {
			window.clearInterval(audio_player.timer);
		}
	},

	show_error : function(audio_id) {
		var audio_player_id = $('#'+audio_id).parent('.audio_player').prop('id');
		var percent_played = audio_player.get_progress(audio_player_id);
		audio_player.clear_progress();

		if ($('#'+audio_id).parents('.audio_player_container').children('p').length) {
			$('#'+audio_id).parents('.audio_player_container').children('p').show();
		} else {
			$('#'+audio_id).parents('.audio_player_container').append(
				$('<p>').text('Sorry, this clip cannot be played at this time. Please try again later.')
			);
		}

		// send error event to ga:
		if (typeof ga === "function") {
			ga('send', 'event', 'audio', 'error', audio_player_id, percent_played);
		}
	},

	get_progress : function(player_id) {
		var duration;
		if (audio_player.has_flash) {
			var ytplayer = document.getElementById("operation_audio_player");
			return Math.floor((ytplayer.getCurrentTime()/ytplayer.getDuration())*100);
		} else {
			var audio_player_elem = document.getElementById("operation_audio_player");
			if (isNaN(audio_player_elem.duration)) {
				var timer = $('#'+player_id).find('.timer .full_length').html();
				var minutes = timer.split(":")[0];
				var seconds = timer.split(":")[1];
				duration = (parseInt(minutes) * 60) + parseInt(seconds);
			} else {
				duration = audio_player_elem.duration;
			}
			return Math.floor((audio_player_elem.currentTime/duration) * 100);
		}
	},

	bind_events : function() {
		if (!audio_player.no_flash_no_html5) {
			$('.play_button').on('click', function(e) {
				e.preventDefault();
				if ($(this).parents('.audio_player_container').children('p').length) { // if there was an error
					$(this).parents('.audio_player_container').children('p').hide();
				}
				var audio_id = $(this).prop('id');
				if (audio_player.has_flash) {
					audio_player.play_flash(audio_id);
				} else {
					audio_player.play_html5(audio_id);
				}
			});
			if (!audio_player.has_flash) {
				$('#operation_audio_player').on('ended', function() {
					this.pause(); // for Opera, since when it returns an ended event, the media isn't paused by default -- Chrome and FF already have a fix for this
					audio_player.clear_progress();
	
					var audio_player_id = $('#'+audio_player.now_playing).parent('.audio_player').prop('id');
					var percent_played = audio_player.get_progress(audio_player_id);
	
					// send ended event to ga:
					if (typeof ga === "function") {
						ga('send', 'event', 'audio', 'ended', audio_player_id, percent_played);
					}
					audio_player.is_ended = true;
				})
				.on('loadstart', function() {
					audio_player.show_loading(-1, $('#'+audio_player.now_playing));
				})
				.on('waiting', function() {
					audio_player.show_loading(-1, $('#'+audio_player.now_playing));
				})
				.on('loadeddata', function() {
					audio_player.show_loading(1, $('#'+audio_player.now_playing));
				})
				.on('progress', function() {
					//get the buffered ranges data
					var ranges = [], buffered_width;
					for(var i = 0; i < this.buffered.length; i++) {
						ranges.push([
							this.buffered.start(i),
							this.buffered.end(i)
						]);
						//buffered_width = (this.buffered.end(i) / this.duration) * 100;
						buffered_width = Math.floor((100 / this.duration) * (ranges[i][1] - ranges[i][0])) + '%';
					}
					$('.playing').parent().find('.buffered').width(buffered_width);
				})
				.on('canplay', function() {
					this.play();
					audio_player.show_loading(1, $('#'+audio_player.now_playing));
					$('#'+audio_player.now_playing).parents('.audio_player_container').children('p').hide(); // if there was an error
					audio_player.is_ended = false;
				})
				.on('stalled', function() {
					if (this.paused) {
						audio_player.show_error(audio_player.now_playing);
					}
				})
				.on('timeupdate', function() {
					var current_time = Math.floor(this.currentTime);
					var minutes = Math.floor(current_time/60);
					var seconds = current_time - (minutes * 60);
					seconds = (seconds < 10) ? "0" + seconds : seconds;
					$('.playing').parent().find('.progress').width((this.currentTime/this.duration)*100+'%');
					$('.playing').parent().find('.timer .elapsed').html(minutes+":"+seconds);
				})
				.on('error', function() {
					audio_player.show_error(audio_player.now_playing);
				});
			}
		} else {
			$('.play_button').on('click', function(e) {
				e.preventDefault();
				return false;
			});
		}
	}, 

	print_html : function(item) {
		var length = (audio_player.has_flash) ? item.youtube_length : item.file_length;
		var minutes = Math.floor(length/60);
		var seconds = length - (minutes * 60);
		seconds = (seconds < 10) ? "0" + seconds : seconds;
		var full_length = minutes+":"+seconds;

		audio_player.audio.push({
			'id' : item.youtube_id,
			'audio_files' : item.audio_files
		});

		var div = 
		$("<div>").prop({
			"class" : "audio_player",
			"id" : "audio_id_" + item.audio_id
		}).append(
			$("<a>").prop({
				"href" : "#",
				"class" : "play_button",
				"id" : item.youtube_id
			})
		);

		if (audio_player.no_flash_no_html5) {
			div.append(
				$("<div>").prop({
					"class" : "inactive"
				}).append(
					$("<p>").html("Unable to play audio")
				)
			);
		} else {
			div.append(
				$("<div>").prop({
					"class" : "progress_bar"
				}).append(
					$("<div>").prop({
						"class" : "progress_wrap"
					}).click(function(e){
						if ($(this).parents('.audio_player').find('a').prop('id') == audio_player.now_playing) {
							audio_player.set_progress($(this), e);
						} else {
							return false;
						}
					}).append(
						$("<div>").prop({
							"class" : "progress"
						}),
						$("<div>").prop({
							"class" : "buffered"
						})
					)
				),
				$("<div>").prop({
					"class" : "timer"
				}).append(
					$("<span>").prop({
						"class" : "elapsed"
					}).html("0:00"),
					"/",
					$("<span>").prop({
						"class" : "full_length"
					}).html(full_length)
				)
			);
		}

		return div;
	},

	set_progress : function(elm, e) {
		var audio_player_id = elm.parent().parent('.audio_player').prop('id');
		var parent_offset = elm.parent().offset(); 
		var parent_play_button = $(elm).parent().siblings('.play_button');
		var rel_X = e.pageX - parent_offset.left;
		var prog_bar_width = $(elm).parent('.progress_bar').width();
		var new_width = rel_X * 100 / prog_bar_width;
		var seek_time = rel_X / prog_bar_width;
		var start_time = '';
		elm.children(".progress").width(new_width+"%");

		if (audio_player.has_flash) {
			var ytplayer = document.getElementById("operation_audio_player");
			start_time = ytplayer.getDuration() * seek_time;
			ytplayer.seekTo(start_time, true);
		} else {
			var audio_player_elem = document.getElementById("operation_audio_player");
			if (isNaN(audio_player_elem.duration)) {
				var last_buffered = audio_player_elem.buffered.end(audio_player_elem.buffered.length-1);
				start_time = last_buffered;
			} else {
				start_time = audio_player_elem.duration * seek_time;
			}
			audio_player_elem.currentTime = Math.floor(start_time);
			parent_play_button.addClass('playing');
			audio_player.show_loading(1, parent_play_button);
		}

		var percent_played = audio_player.get_progress(audio_player_id);

		var minutes = Math.floor(start_time/60);
		var seconds = Math.floor(start_time) - (minutes * 60);
		seconds = (seconds < 10) ? "0" + seconds : seconds;
		$('.playing').parent().find('.timer .elapsed').html(minutes+":"+seconds);
		if (audio_player.timer) {
			window.clearInterval(audio_player.timer);
		}

		// send seek event to ga:
		if (typeof ga === "function") {
			ga('send', 'event', 'audio', 'seek', audio_player_id, percent_played);
		}
	},

	on_yt_player_state_change : function(new_state) {
		var ytplayer = document.getElementById("operation_audio_player");
		var current_audio_id = $('#operation_audio_player').find('param[name="audio_id"]').val();
		audio_player.show_loading(new_state, $('#'+current_audio_id));
		if (new_state == 0) {
			var audio_player_id = $('#'+current_audio_id).parent('.audio_player').prop('id');
			var percent_played = audio_player.get_progress(audio_player_id);

			audio_player.clear_progress();

			// send ended event to ga:
			if (typeof ga === "function") {
				ga('send', 'event', 'audio', 'ended', audio_player_id, percent_played);
			}
			audio_player.is_ended = true;
		} else if (new_state == -1) { // unstarted (loading)
			if ($('.playing').length) { // show that the video is loading, progress is restarting
				$('.playing').parent().find('.progress').width('0%');
				$('.playing').parent().find('.timer .elapsed').html("0:00");
			}
		} else if (new_state == 1) { // playing
			if (audio_player.timer) {
				window.clearInterval(audio_player.timer);
			}
			audio_player.timer = window.setInterval(function() {
				var current_time = Math.floor(ytplayer.getCurrentTime());
				var minutes = Math.floor(current_time/60);
				var seconds = current_time - (minutes * 60);
				seconds = (seconds < 10) ? "0" + seconds : seconds;
				$('.playing').parent().find('.progress').width((ytplayer.getCurrentTime()/ytplayer.getDuration())*100+'%');
				$('.playing').parent().find('.timer .elapsed').html(minutes+":"+seconds);
				$('.playing').parent().find('.buffered').width(ytplayer.getVideoLoadedFraction()*100+'%');
			},1000);
			audio_player.is_ended = false;
		}
	},

	on_yt_player_error : function(error_code) {
		var current_audio_id = $('#operation_audio_player').find('param[name="audio_id"]').val();

		audio_player.show_error(current_audio_id);
	}
};

// function required by youtube js api
function onYouTubePlayerReady(playerId) {
	var ytplayer = document.getElementById(playerId);
	ytplayer.addEventListener("onStateChange", "audio_player.on_yt_player_state_change");
	ytplayer.addEventListener("onError", "audio_player.on_yt_player_error");
}